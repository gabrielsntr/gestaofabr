﻿using GestaoFABR.Model;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoFABR.Control
{
    class ControlContaPagarReceber
    {
        private Conexao conexao = new Conexao();
        public DataTable obter()
        {
            try
            {
                conexao.conectar();
                MySqlDataAdapter da = new MySqlDataAdapter("select c.id, c.tipo, c.valor, c.historico, c.data_emissao, c.pessoa_id, p.nome from conta_pagar_receber as c join pessoa as p on(c.pessoa_id = p.id);", conexao.getConexao());
                DataTable dt = new DataTable();
                dt.Clear();
                da.Fill(dt);
                conexao.encerrar();
                return dt;

            }
            catch (Exception exc)
            {
                Console.WriteLine("Erro ao executar a query: " + exc.ToString());
                return null;
            }

        }

        public Boolean atualiza(ContaPagarReceber c)
        {
            try
            {
                conexao.conectar();
                MySqlCommand update = new MySqlCommand("update conta_pagar_receber set tipo = '" + c.tipo + "', valor = " + c.valor + ", historico = '" + c.historico + "', data_emissao = CAST('" + c.data_emissao.ToString("yyyy-MM-dd") + "' as date), pessoa_id = " + c.pessoa_id + " where id = '" + c.id + ";", conexao.getConexao());
                if (update.ExecuteNonQuery() > 0)
                {
                    conexao.encerrar();
                    return true;
                }
                else
                {
                    conexao.encerrar();
                    return false;
                }
            }
            catch (Exception exc)
            {
                Console.WriteLine("Erro ao executar a query: " + exc.ToString());
                conexao.encerrar();
                return false;
            }

        }

        public bool insere(ContaPagarReceber c)
        {
            try
            {
                conexao.conectar();
                MySqlCommand update = new MySqlCommand("insert into conta_pagar_receber VALUES (DEFAULT, '"
                    + c.tipo + "', " + c.valor + ", '" + c.historico + "', CAST('" + c.data_emissao.ToString("yyyy-MM-dd") + " as Date), " + c.pessoa_id + ");", conexao.getConexao());
                if (update.ExecuteNonQuery() > 0)
                {
                    conexao.encerrar();
                    return true;
                }
                else
                {
                    conexao.encerrar();
                    return false;
                }
            }
            catch (Exception exc)
            {
                Console.WriteLine("Erro ao executar a query: " + exc.ToString());
                conexao.encerrar();
                return false;
            }

        }

        public bool remove(int Id)
        {
            try
            {
                conexao.conectar();
                MySqlCommand update = new MySqlCommand("delete from conta_pagar_receber where id = " + Id + ";", conexao.getConexao());
                if (update.ExecuteNonQuery() > 0)
                {
                    conexao.encerrar();
                    return true;
                }
                else
                {
                    conexao.encerrar();
                    return false;
                }
            }
            catch (Exception exc)
            {
                Console.WriteLine("Erro ao executar a query: " + exc.ToString());
                conexao.encerrar();
                return false;
            }

        }
    }
}
