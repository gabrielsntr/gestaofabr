﻿using GestaoFABR.Model;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoFABR.Control
{
    class ControlMovimentacaoFinanceira
    {
        private Conexao conexao = new Conexao();
        public DataTable obter()
        {
            try
            {
                conexao.conectar();
                MySqlDataAdapter da = new MySqlDataAdapter("select m.id, m.data, m.tipo, (CASE m.tipo when 'P' then 'A Pagar' when 'R' then 'A Receber' END) as desc_tipo, m.valor, m.beneficiario, m.historico, m.conta_bancaria_id, c.descricao as desc_conta_bancaria, m.parcela_pagar_receber_id from mov_financeira as m join conta_bancaria as c on(m.conta_bancaria_id = c.id);", conexao.getConexao());
                DataTable dt = new DataTable();
                dt.Clear();
                da.Fill(dt);
                conexao.encerrar();
                return dt;

            }
            catch (Exception exc)
            {
                Console.WriteLine("Erro ao executar a query: " + exc.ToString());
                return null;
            }

        }

        public Boolean atualiza(MovimentacaoFinanceira m)
        {
            try
            {
                conexao.conectar();
                MySqlCommand update = new MySqlCommand("update mov_financeira set " +
                    "data = CAST('" + m.data.ToString("yyyy-MM-dd") + "' as Date), " +
                    "tipo = '" + m.tipo + "', " +
                    "valor = " + m.valor + ", " +
                    "beneficiario = '" + m.beneficiario + "', " +
                    "historico = '" + m.historico + "', " +
                    "conta_bancaria_id = " + m.conta_bancaria_id + ", " +
                    "parcela_pagar_receber_id = " + m.parcela_pagar_receber_id + " " +
                    "where id = '" + m.id + ";", conexao.getConexao());
                if (update.ExecuteNonQuery() > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception exc)
            {
                Console.WriteLine("Erro ao executar a query: " + exc.ToString());
                return false;
            }

        }

        public bool insere(MovimentacaoFinanceira m)
        {
            try
            {
                conexao.conectar();
                MySqlCommand update = new MySqlCommand("insert into mov_financeira VALUES (DEFAULT, CAST('" + m.data.ToString("yyyy-MM-dd") + "' as Date), '" +
                    m.tipo + "', " + 
                    m.valor + ", '" +
                    m.beneficiario + "', '" +
                    m.historico + "', " +
                    m.conta_bancaria_id + ", " +
                    m.parcela_pagar_receber_id + ");", conexao.getConexao());
                if (update.ExecuteNonQuery() > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception exc)
            {
                Console.WriteLine("Erro ao executar a query: " + exc.ToString());
                return false;
            }

        }

        public bool remove(int Id)
        {
            try
            {
                conexao.conectar();
                MySqlCommand update = new MySqlCommand("delete from mov_financeira where id = " + Id + ";", conexao.getConexao());
                if (update.ExecuteNonQuery() > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception exc)
            {
                Console.WriteLine("Erro ao executar a query: " + exc.ToString());
                return false;
            }

        }
    }
}
