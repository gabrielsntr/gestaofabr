﻿using GestaoFABR.Model;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoFABR.Control
{
    class ControlParcelaPagarReceber
    {
        private Conexao conexao = new Conexao();
        public DataTable obter()
        {
            try
            {
                conexao.conectar();
                MySqlDataAdapter da = new MySqlDataAdapter("select p.id, p.numero, p.valor, p.data_vencimento, p.situacao, (case p.situacao when 'A' then 'Em Aberto' when 'P' then 'Parcial' when 'B' then 'Baixado' when 'C' then 'Cancelado' END) as desc_situacao, p.conta_pagar_receber_id from parcela_pagar_receber as p;", conexao.getConexao());
                DataTable dt = new DataTable();
                dt.Clear();
                da.Fill(dt);
                conexao.encerrar();
                return dt;

            }
            catch (Exception exc)
            {
                Console.WriteLine("Erro ao executar a query: " + exc.ToString());
                return null;
            }

        }

        public Boolean atualiza(ParcelaPagarReceber c)
        {
            try
            {
                conexao.conectar();
                MySqlCommand update = new MySqlCommand("update parcela_pagar_receber set numero = " + c.numero + ", valor = " + c.valor + ", data_vencimento = '" + c.data_vencimento.ToString("yyyy-MM-dd") + "', situacao = '" + c.situacao + "' where id = '" + c.id + ";", conexao.getConexao());
                if (update.ExecuteNonQuery() > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception exc)
            {
                Console.WriteLine("Erro ao executar a query: " + exc.ToString());
                return false;
            }

        }

        public bool insere(ParcelaPagarReceber c)
        {
            try
            {
                conexao.conectar();
                MySqlCommand update = new MySqlCommand("insert into parcela_pagar_receber VALUES (DEFAULT, " + c.numero + ", " + c.valor + ", CAST('" + c.data_vencimento.ToString("yyyy-MM-dd") + "' as date), '" + c.situacao + "');", conexao.getConexao());
                if (update.ExecuteNonQuery() > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception exc)
            {
                Console.WriteLine("Erro ao executar a query: " + exc.ToString());
                return false;
            }

        }

        public bool remove(int Id)
        {
            try
            {
                conexao.conectar();
                MySqlCommand update = new MySqlCommand("delete from parcela_pagar_receber where id = " + Id + ";", conexao.getConexao());
                if (update.ExecuteNonQuery() > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception exc)
            {
                Console.WriteLine("Erro ao executar a query: " + exc.ToString());
                return false;
            }

        }
    }
}
