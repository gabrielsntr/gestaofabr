﻿using GestaoFABR.Control;
using GestaoFABR.Model;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoFABR.Control
{
    class ControlLocalTreino
    {
        private Conexao conexao = new Conexao();

        public ControlLocalTreino()
        {
        }

        public DataTable obter()
        {
            try
            {
                conexao.conectar();
                MySqlDataAdapter da = new MySqlDataAdapter("select * from local_treino;", conexao.getConexao());
                DataTable dt = new DataTable();
                dt.Clear();
                da.Fill(dt);
                conexao.encerrar();
                return dt;

            } catch (Exception exc)
            {
                Console.WriteLine("Erro ao executar a query: " + exc.ToString());
                conexao.encerrar();
                return null;
            } 
            
        }
        public string obterNome(int id)
        {
            try
            {
                conexao.conectar();
                MySqlCommand select = new MySqlCommand("select nome from local_treino where id = " + id + ";", conexao.getConexao());
                MySqlDataReader dr;
                dr = select.ExecuteReader();
                string result = "";
                while (dr.Read())
                {
                    result = dr.GetString(0);
                }
                conexao.encerrar();
                return result;

            }
            catch (Exception exc)
            {
                Console.WriteLine("Erro ao executar a query: " + exc.ToString());
                return null;
            }

        }
        public Boolean atualiza(LocalTreino l)
        {
            try
            {
                conexao.conectar();
                MySqlCommand update = new MySqlCommand("update local_treino set nome = '"
                    + l.nome + "', logradouro = '" + l.logradouro + "', numero = '" + l.numero + "', bairro = '" + l.bairro + "' where id = " + l.id + ";", conexao.getConexao());
                if (update.ExecuteNonQuery() > 0)
                {
                    conexao.encerrar();
                    return true;
                } else
                {
                    conexao.encerrar();
                    return false;
                }
            }
            catch (Exception exc)
            {
                Console.WriteLine("Erro ao executar a query: " + exc.ToString());
                conexao.encerrar();
                return false;
            } 

        }

        public bool insere(LocalTreino l)
        {
            try
            {
                conexao.conectar();
                MySqlCommand update = new MySqlCommand("insert into local_treino VALUES (DEFAULT, '"
                    + l.nome + "', '" + l.logradouro + "', '" + l.numero + "', '" + l.bairro + "');", conexao.getConexao());
                if (update.ExecuteNonQuery() > 0)
                {
                    conexao.encerrar();
                    return true;
                }
                else
                {
                    conexao.encerrar();
                    return false;
                }
            }
            catch (Exception exc)
            {
                Console.WriteLine("Erro ao executar a query: " + exc.ToString());
                conexao.encerrar();
                return false;
            }

        }

        public bool remove(int Id)
        {
            try
            {
                conexao.conectar();
                MySqlCommand update = new MySqlCommand("delete from local_treino where id = " + Id + ";", conexao.getConexao());
                if (update.ExecuteNonQuery() > 0)
                {
                    conexao.encerrar();
                    return true;
                }
                else
                {
                    conexao.encerrar();
                    return false;
                }
            }
            catch (Exception exc)
            {
                Console.WriteLine("Erro ao executar a query: " + exc.ToString());
                conexao.encerrar();
                return false;
            }

        }
    }
}
