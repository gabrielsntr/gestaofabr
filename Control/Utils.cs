﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestaoFABR.Control
{
    class Utils
    {
        public static void filtraGrid(DataGridView dg, ComboBox cb, TextBox tb)
        {
            if (cb.SelectedIndex >= 0)
            {
                string rowFilter;
                if (!string.IsNullOrWhiteSpace(tb.Text))
                {
                    if (cb.SelectedIndex > 0)
                    {
                        rowFilter = string.Format("[{0}] LIKE '%{1}%'", cb.SelectedItem.ToString(), tb.Text);

                    }
                    else
                    {
                        rowFilter = string.Format("[{0}] = {1}", cb.SelectedItem.ToString(), tb.Text);
                    }
                    (dg.DataSource as DataTable).DefaultView.RowFilter = rowFilter;
                    dg.Refresh();
                }
                else
                {
                    (dg.DataSource as DataTable).DefaultView.RowFilter = null;
                    dg.Refresh();
                }
            }
        }
        
        public static DateTime stringToDtPicker(string data)
        {
           return DateTime.ParseExact(data, ("dd/MM/yyyy"), new CultureInfo("pt-BR"));
        }

        public static DateTime stringToDtPickerHora(string hora)
        {
            return DateTime.ParseExact(hora, ("HH:mm"), new CultureInfo("pt-BR"));
        }

        public static void formataNumericEvent(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != ','))
            {
                e.Handled = true;
            }
            // only allow one decimal point
            if ((e.KeyChar == ',') && ((sender as MaskedTextBox).Text.IndexOf(',') > -1))
            {
                e.Handled = true;
            }
        }

        public static void formataNumericEventTextBox(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != ','))
            {
                e.Handled = true;
            }
            // only allow one decimal point
            if ((e.KeyChar == ',') && ((sender as TextBox).Text.IndexOf(',') > -1))
            {
                e.Handled = true;
            }
        }

        public static string unformatCPF_CNPJ(string cpf_cnpj)
        {
            return cpf_cnpj.Replace(".", "").Replace("/", "").Replace("-", "");
        }
        
        public static bool verificaDataHora(DateTime d1, DateTime d2)
        {
            if (d2 >= d1)
            {
                return true;
            } else
            {
                return false;
            }
            
        }
        
    }
}
