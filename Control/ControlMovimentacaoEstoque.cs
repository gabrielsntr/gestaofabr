﻿using GestaoFABR.Model;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoFABR.Control
{
    class ControlMovimentacaoEstoque
    {
        private Conexao conexao = new Conexao();
        public DataTable obter()
        {
            try
            {
                conexao.conectar();
                MySqlDataAdapter da = new MySqlDataAdapter("select m.id, m.tipo, m.valor, m.historico, m.data_emissao, m.pessoa_id, p.nome from mov_estoque as m join produto as p on(m.produto_id = p.id);", conexao.getConexao());
                DataTable dt = new DataTable();
                dt.Clear();
                da.Fill(dt);
                conexao.encerrar();
                return dt;

            }
            catch (Exception exc)
            {
                Console.WriteLine("Erro ao executar a query: " + exc.ToString());
                conexao.encerrar();
                return null;
            }

        }

        public Boolean atualiza(MovimentacaoEstoque m)
        {
            try
            {
                conexao.conectar();
                MySqlCommand update = new MySqlCommand("update mov_estoque set data = CAST('" + m.data.ToString("yyyy-MM-dd") + "' as Date), tipo_movimento = '" + m.tipo_movimento + "', quantidade = " + m.quantidade + ", valor = " + m.valor + ", produto_id = " + m.produto_id + " where id = '" + m.id + ";", conexao.getConexao());
                if (update.ExecuteNonQuery() > 0)
                {
                    conexao.encerrar();
                    return true;
                }
                else
                {
                    conexao.encerrar();
                    return false;
                }
            }
            catch (Exception exc)
            {
                Console.WriteLine("Erro ao executar a query: " + exc.ToString());
                conexao.encerrar();
                return false;
            }

        }

        public bool insere(MovimentacaoEstoque m)
        {
            try
            {
                conexao.conectar();
                MySqlCommand update = new MySqlCommand("insert into mov_estoque VALUES (DEFAULT, '" +
                    "CAST('" + m.data.ToString("yyyy-MM-dd") + "' as Date), '" + m.tipo_movimento + "', " + m.quantidade + ", " + m.valor + ", " + m.produto_id + ");", conexao.getConexao());
                if (update.ExecuteNonQuery() > 0)
                {
                    conexao.encerrar();
                    return true;
                }
                else
                {
                    conexao.encerrar();
                    return false;
                }
            }
            catch (Exception exc)
            {
                Console.WriteLine("Erro ao executar a query: " + exc.ToString());
                return false;
            }

        }

        public bool remove(int Id)
        {
            try
            {
                conexao.conectar();
                MySqlCommand update = new MySqlCommand("delete from mov_estoque where id = " + Id + ";", conexao.getConexao());
                if (update.ExecuteNonQuery() > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception exc)
            {
                Console.WriteLine("Erro ao executar a query: " + exc.ToString());
                return false;
            }

        }
    }
}
