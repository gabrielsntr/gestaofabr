﻿using GestaoFABR.Model;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoFABR.Control
{
    class ControlContaBancaria
    {
        private Conexao conexao = new Conexao();

        public ControlContaBancaria()
        {
        }

        public DataTable obter()
        {
            try
            {
                conexao.conectar();
                DataTable dt = conexao.selecionar("Select id, descricao, tipo, (CASE tipo when 'C' then 'Conta Corrente' when 'P' then 'Conta Poupança' when 'R' then 'Caixa Registradora' END) as descricao_tipo, conta, agencia, beneficiario from conta_bancaria;");
                conexao.encerrar();
                return dt;

            }
            catch (Exception exc)
            {
                Console.WriteLine("Erro ao executar a query: " + exc.ToString());
                return null;
            }

        }

        public Boolean atualiza(ContaBancaria c)
        {
            try
            {
                conexao.conectar();
                MySqlCommand update = new MySqlCommand("update conta_bancaria set descricao = '"
                    + c.descricao + "', tipo = '" + c.tipo + "', conta = '" + c.conta + "', agencia = '" + c.agencia + "', beneficiario = '" + c.beneficiario + "' where id = " + c.id + ";", conexao.getConexao());
                if (update.ExecuteNonQuery() > 0)
                {
                    conexao.encerrar();
                    return true;
                }
                else
                {
                    conexao.encerrar();
                    return false;
                }
            }
            catch (Exception exc)
            {
                Console.WriteLine("Erro ao executar a query: " + exc.ToString());
                conexao.encerrar();
                return false;
            }

        }

        public bool insere(ContaBancaria c)
        {
            try
            {
                conexao.conectar();
                MySqlCommand update = new MySqlCommand("insert into conta_bancaria VALUES (DEFAULT, '"
                    + c.descricao+ "', '" + c.tipo + "', '" + c.conta + "', '" + c.agencia + "', '" + c.beneficiario + "');", conexao.getConexao());
                if (update.ExecuteNonQuery() > 0)
                {
                    conexao.encerrar();
                    return true;
                }
                else
                {
                    conexao.encerrar();
                    return false;
                }
            }
            catch (Exception exc)
            {
                Console.WriteLine("Erro ao executar a query: " + exc.ToString());
                conexao.encerrar();
                return false;
            }

        }

        public bool remove(int Id)
        {
            try
            {
                conexao.conectar();
                MySqlCommand update = new MySqlCommand("delete from conta_bancaria where id = " + Id + ";", conexao.getConexao());
                if (update.ExecuteNonQuery() > 0)
                {
                    conexao.encerrar();
                    return true;
                }
                else
                {
                    conexao.encerrar();
                    return false;
                }
            }
            catch (Exception exc)
            {
                Console.WriteLine("Erro ao executar a query: " + exc.ToString());
                return false;
            }

        }
        public char retornaTipoChar(int cbId)
        {
            char result = new char();
            switch (cbId)
            {
                case 0:
                    result = 'C';
                    break;
                case 1:
                    result = 'P';
                    break;
                case 2:
                    result = 'R';
                    break;
                default:
                    break;
            }
            return result;
        }
        public int retornaTipoInt(char tipo)
        {
            int result = -1;
            switch (tipo)
            {
                case 'C':
                    result = 0;
                    break;
                case 'P':
                    result = 1;
                    break;
                case 'R':
                    result = 2;
                    break;
                default:
                    break;
            }
            return result;
        }
    }
}
