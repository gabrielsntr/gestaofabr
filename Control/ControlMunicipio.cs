﻿using GestaoFABR.Model;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoFABR.Control
{
    class ControlMunicipio
    {
        private Conexao conexao = new Conexao();
        public DataTable obter()
        {
            try
            {
                conexao.conectar();
                MySqlDataAdapter da = new MySqlDataAdapter("select * from municipio;", conexao.getConexao());
                DataTable dt = new DataTable();
                dt.Clear();
                da.Fill(dt);
                conexao.encerrar();
                return dt;

            }
            catch (Exception exc)
            {
                Console.WriteLine("Erro ao executar a query: " + exc.ToString());
                return null;
            }

        }
        public string obterNome(int id)
        {
            try
            {
                conexao.conectar();
                MySqlCommand select = new MySqlCommand("select CONCAT(nome, '/', uf) from municipio where id = " + id + ";", conexao.getConexao());
                MySqlDataReader dr;
                dr = select.ExecuteReader();
                string result = "";
                while (dr.Read())
                {
                    result = dr.GetString(0);
                }
                conexao.encerrar();
                return result;

            }
            catch (Exception exc)
            {
                Console.WriteLine("Erro ao executar a query: " + exc.ToString());
                return null;
            }

        }

        public Boolean atualiza(Municipio m)
        {
            try
            {
                conexao.conectar();
                MySqlCommand update = new MySqlCommand("update municipio set nome = '"
                    + m.nome + "', uf = '" + m.uf + "' where id = " + m.id + ";", conexao.getConexao());
                if (update.ExecuteNonQuery() > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception exc)
            {
                Console.WriteLine("Erro ao executar a query: " + exc.ToString());
                return false;
            }

        }

        public bool insere(Municipio m)
        {
            try
            {
                conexao.conectar();
                MySqlCommand update = new MySqlCommand("insert into municipio VALUES (DEFAULT, '"
                    + m.nome + "', '" + m.uf + "');", conexao.getConexao());
                if (update.ExecuteNonQuery() > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception exc)
            {
                Console.WriteLine("Erro ao executar a query: " + exc.ToString());
                return false;
            }

        }

        public bool remove(int Id)
        {
            try
            {
                conexao.conectar();
                MySqlCommand update = new MySqlCommand("delete from municipio where id = " + Id + ";", conexao.getConexao());
                if (update.ExecuteNonQuery() > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception exc)
            {
                Console.WriteLine("Erro ao executar a query: " + exc.ToString());
                return false;
            }

        }

    }
}
