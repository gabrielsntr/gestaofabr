﻿using GestaoFABR.Model;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoFABR.Control
{
    class ControlPessoa
    {
        private Conexao conexao = new Conexao();
        public DataTable obter()
        {
            try
            {
                conexao.conectar();
                //MySqlDataAdapter da = new MySqlDataAdapter("SELECT p.id, p.nome, p.cpf, p.rg, p.cep, p.logradouro, p.numero, p.bairro, p.municipio_id, m.nome,  FROM pessoa p left JOIN municipio AS m ON m.id = p.municipio_id; ", conexao.getConexao());
                MySqlDataAdapter da = new MySqlDataAdapter(
                    "SELECT p.id, " +
                        "p.nome, " +
                        "p.tipo_pessoa, " +
                        "(CASE p.tipo_pessoa when 'J' then p.cnpj else p.cpf END) as cnpj_cpf, " +
                        "(CASE p.tipo_pessoa when 'J' then p.insc_est else p.rg END) as rg_insc_est, " +
                        "date_format(data_nascimento, '%d/%m/%Y') as data_nascimento," +
                        "p.cep, " +
                        "p.logradouro, " +
                        "p.numero, " +
                        "p.bairro, " +
                        "p.municipio_id, " +
                        "CONCAT(m.nome, '/', m.uf) as municipio_nome, " +
                        "(CASE p.atleta WHEN 0 THEN 'Não' WHEN 1 then 'Sim' END) AS atleta, " +
                        "p.peso, " +
                        "p.altura, " +
                        "p.tam_helmet, " +
                        "p.tam_shoulder, " +
                        "p.tam_jersey, " +
                        "p.tam_calca, " +
                        "p.tam_pe, " +
                        "(CASE p.isento_mensalidade WHEN 0 THEN 'Não' WHEN 1 then 'Sim' END) AS isento_mensalidade, " +
                        "(CASE p.ativo WHEN 0 THEN 'Não' WHEN 1 then 'Sim' END) AS ativo, " +
                        "p.nacionalidade_id," +
                        "n.nome as nacionalidade " +
                    "FROM pessoa AS p " +
                    "LEFT JOIN municipio AS m ON m.id = p.municipio_id " +
                    "LEFT JOIN nacionalidade AS n ON n.id = p.nacionalidade_id " +
                    "order by p.id desc;", conexao.getConexao());

                DataTable dt = new DataTable();
                dt.Clear();
                da.Fill(dt);
                conexao.encerrar();
                return dt;

            }
            catch (Exception exc)
            {
                Console.WriteLine("Erro ao executar a query: " + exc.ToString());
                return null;
            }

        }

        public string obterNome(int id)
        {
            try
            {
                conexao.conectar();
                MySqlCommand select = new MySqlCommand("select nome from pessoa where id = " + id + ";", conexao.getConexao());
                MySqlDataReader dr;
                dr = select.ExecuteReader();
                string result = "";
                while (dr.Read())
                {
                    result = dr.GetString(0);
                }
                conexao.encerrar();
                return result;

            }
            catch (Exception exc)
            {
                Console.WriteLine("Erro ao executar a query: " + exc.ToString());
                return null;
            }

        }

        public List<int> obterAtletas()
        {
            try
            {
                conexao.conectar();
                MySqlCommand select = new MySqlCommand("select id from pessoa where atleta = 1 and ativo = 1;", conexao.getConexao());
                MySqlDataReader dr;
                dr = select.ExecuteReader();
                List <int> result = new List<int>();
                while (dr.Read())
                {
                    result.Add(dr.GetInt32(0));
                }
                conexao.encerrar();
                return result;

            }
            catch (Exception exc)
            {
                Console.WriteLine("Erro ao executar a query: " + exc.ToString());
                return null;
            }

        }
        public bool atualiza(Pessoa m)
        {
            try
            {
                conexao.conectar();
                string sql, cpf, cnpj, id_municipio, id_nacionalidade, peso, altura;
                cpf = "'" + Utils.unformatCPF_CNPJ(m.cpf) + "'";
                cnpj = "'" + Utils.unformatCPF_CNPJ(m.cnpj) + "'";
                id_municipio = m.municipio_id.ToString();
                id_nacionalidade = m.nacionalidade_id.ToString();
                peso = m.peso.ToString().Replace(",",".");
                altura = m.altura.ToString().Replace(",", ".");

                if (String.IsNullOrWhiteSpace(Utils.unformatCPF_CNPJ(m.cpf).Trim()))
                {
                    cpf = "null";
                }
                if (String.IsNullOrWhiteSpace(Utils.unformatCPF_CNPJ(m.cnpj).Trim()))
                {
                    cnpj = "null";
                }
                if (m.municipio_id == 0)
                {
                    id_municipio = "null";
                }
                if (m.nacionalidade_id == 0)
                {
                    id_nacionalidade = "null";
                }
                if (m.peso == 0)
                {
                    peso = "null";
                }
                if (m.altura == 0)
                {
                    altura = "null";
                }
                sql = "update pessoa set " +
                    "nome = '" + m.nome +
                    "', tipo_pessoa = '" + m.tipo_pessoa +
                    "', data_nascimento = CAST('" + m.data_nascimento.ToString("yyyy-MM-dd") + "' as Date)" +
                    ", cpf = " + cpf +
                    ", rg = '" + m.rg +
                    "', cnpj = " + cnpj +
                    ", insc_est = '" + m.insc_est +
                    "', cep = '" + Utils.unformatCPF_CNPJ(m.cep).Trim() +
                    "', logradouro = '" + m.logradouro +
                    "', numero = '" + m.numero +
                    "', bairro = '" + m.bairro +
                    "', municipio_id = " + id_municipio +
                    ", atleta = " + m.isAtleta +
                    ", peso = " + peso +
                    ", altura = " + altura +
                    ", tam_helmet = '" + m.tam_helmet +
                    "', tam_shoulder = '" + m.tam_shoulder +
                    "', tam_jersey = '" + m.tam_jersey +
                    "', tam_pe = " + m.tam_pe +
                    ", isento_mensalidade = " + m.isento_mensalidade +
                    ", ativo = " + m.ativo +
                    ", nacionalidade_id = " + id_nacionalidade +
                    " where id = " + m.id + ";";
                    MySqlCommand update = new MySqlCommand(sql, conexao.getConexao());
                int res = update.ExecuteNonQuery();
                if (res > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception exc)
            {
                Console.WriteLine("Erro ao executar a query: " + exc.ToString());
                return false;
            } finally
            {
                conexao.encerrar();
            }


        }

        public int insere(Pessoa m) 
        {
            int id_inserido = -1;
            string sql, cpf, cnpj, id_municipio, id_nacionalidade, peso, altura;
            cpf = "'" + Utils.unformatCPF_CNPJ(m.cpf) + "'";
            cnpj = "'" + Utils.unformatCPF_CNPJ(m.cnpj) + "'";
            id_municipio = m.municipio_id.ToString();
            id_nacionalidade = m.nacionalidade_id.ToString();
            peso = m.peso.ToString();
            altura = m.altura.ToString();

            if (String.IsNullOrWhiteSpace(Utils.unformatCPF_CNPJ(m.cpf).Trim()))
            {
                cpf = "null";
            }
            if (String.IsNullOrWhiteSpace(Utils.unformatCPF_CNPJ(m.cnpj).Trim()))
            {
                cnpj = "null";
            }
            if (m.municipio_id == 0)
            {
                id_municipio = "null";
            }
            if (m.nacionalidade_id == 0)
            {
                id_nacionalidade = "null";
            }
            if (m.peso == 0)
            {
                peso = "null";
            }
            if (m.altura == 0)
            {
                altura = "null";
            }
            sql = "insert into pessoa VALUES (DEFAULT, '" + 
                m.nome + "', '" + 
                m.tipo_pessoa + "'," + 
                cpf + ", '" + 
                m.rg + "', CAST('" + 
                m.data_nascimento.ToString("yyyy-MM-dd") + "' as Date), " + 
                cnpj + ",'" + 
                m.insc_est + "', '" + 
                Utils.unformatCPF_CNPJ(m.cep) + "', '" + 
                m.logradouro + "', '" + 
                m.numero + "', '" + 
                m.bairro + "', " + 
                m.municipio_id + ", " +
                m.isAtleta + ", " +
                peso.Replace(',', '.') + ", " +
                altura.Replace(',', '.') + ", '" +
                m.tam_helmet + "', '" +
                m.tam_shoulder + "', '" +
                m.tam_jersey + "', '" +
                m.tam_calca + "', " +
                m.tam_pe + ", " +
                m.isento_mensalidade + ", " +
                m.ativo + ", " +
                id_nacionalidade + ");";
            try
            {                
                conexao.conectar();
                MySqlCommand update = new MySqlCommand(sql, conexao.getConexao());
                if (update.ExecuteNonQuery() > 0)
                {
                    MySqlCommand select = new MySqlCommand("select LAST_INSERT_ID();", conexao.getConexao());
                    MySqlDataReader dr;
                    dr = select.ExecuteReader();                    
                    while (dr.Read())
                    {
                        id_inserido = dr.GetInt32(0);
                    }
                }
                else
                {
                }
            } catch (Exception exc)
            {
                Console.WriteLine("Erro ao executar a query: " + exc.ToString());
            }
            return id_inserido;
        }

        public bool remove(int Id)
        {
            try
            {
                conexao.conectar();
                MySqlCommand update = new MySqlCommand("delete from pessoa where id = " + Id + ";", conexao.getConexao());
                if (update.ExecuteNonQuery() > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception exc)
            {
                Console.WriteLine("Erro ao executar a query: " + exc.ToString());
                return false;
            }

        }

        public char retornaTipoPessoaChar(int cbId)
        {
            char result = new char();
            switch (cbId)
            {
                case 0:
                    result = 'F';
                    break;
                case 1:
                    result = 'J';
                    break;
                default:
                    break;
            }
            return result;
        }
        public int retornaTipoPessoaInt(char tipo)
        {
            int result = -1;
            switch (tipo)
            {
                case 'F':
                    result = 0;
                    break;
                case 'J':
                    result = 1;
                    break;
                default:
                    break;
            }
            return result;
        }

        public string retornaTamanhoString(int cbId)
        {
            string result = "";
            switch (cbId)
            {
                case 0:
                    result = "S";
                    break;
                case 1:
                    result = "M";
                    break;
                case 2:
                    result = "L";
                    break;
                case 3:
                    result = "XL";
                    break;
                case 4:
                    result = "XXL";
                    break;
                case 5:
                    result = "XXXL";
                    break;
            }
            return result;
        }
        public int retornaTamanhoInt(string tam)
        {
            int result = -1;
            switch (tam)
            {
                case "S":
                    result = 0;
                    break;
                case "M":
                    result = 1;
                    break;
                case "L":
                    result = 2;
                    break;
                case "XL":
                    result = 3;
                    break;
                case "XXL":
                    result = 4;
                    break;
                case "XXXL":
                    result = 5;
                    break;
                default:
                    break;
            }
            return result;
        }

    }
}
