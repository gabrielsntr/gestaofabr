﻿using GestaoFABR.Model;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoFABR.Control
{
    class ControlAgenda
    {
        private Conexao conexao = new Conexao();
        public DataTable obter()
        {
            try
            {
                conexao.conectar();
                MySqlDataAdapter da = new MySqlDataAdapter("select 	a.id," +
                    "date_format(a.data_inicio, '%d/%m/%Y') as data_inicio, " +
                    "date_format(a.hora_inicio, '%H:%i') as hora_inicio," +
                    "date_format(a.data_fim, '%d/%m/%Y') as data_fim, " +
                    "date_format(a.hora_fim, '%H:%i') as hora_fim," +
                    "a.descricao, " +
                    "a.tipo, " +
                    "(CASE a.tipo when 'J' then 'Jogo' when 'T' then 'Treino' when 'R' then 'Reunião' when 'E' then 'Evento' else 'Outro' END) as desc_tipo, " +
                    "a.observacao, " +
                    "a.local_treino_id, " +
                    "l.nome " +
                    "from agenda as a left join local_treino as l on(a.local_treino_id = l.id);", conexao.getConexao());
                DataTable dt = new DataTable();
                dt.Clear();
                da.Fill(dt);
                conexao.encerrar();
                return dt;

            }
            catch (Exception exc)
            {
                Console.WriteLine("Erro ao executar a query: " + exc.ToString());
                conexao.encerrar();
                return null;
            }

        }


        public string obterNome(int id)
        {
            try
            {
                conexao.conectar();
                MySqlCommand select = new MySqlCommand("select descricao from agenda where id = " + id + ";", conexao.getConexao());
                MySqlDataReader dr;
                dr = select.ExecuteReader();
                string result = "";
                while (dr.Read())
                {
                    result = dr.GetString(0);
                }
                conexao.encerrar();
                return result;

            }
            catch (Exception exc)
            {
                Console.WriteLine("Erro ao executar a query: " + exc.ToString());
                return null;
            }

        }

        public Boolean atualiza(Agenda a)
        {
            try
            {
                conexao.conectar();
                MySqlCommand update = new MySqlCommand("update agenda set descricao = '" + 
                    a.descricao + "', tipo = '" + 
                    a.tipo + "', data_inicio = CAST('" + 
                    a.data_inicio.ToString("yyyy-MM-dd") + "' as Date), " +
                    "hora_inicio = CAST('" + 
                    a.hora_inicio.ToString("HH:mm") 
                    + "' as TIME), data_fim = CAST('" + 
                    a.data_fim.ToString("yyyy-MM-dd") 
                    + "' as Date), " +
                    "hora_fim = CAST('" 
                    + a.hora_fim.ToString("HH:mm") + "' as TIME), observacao = '" 
                    + a.observacao + "', local_treino_id = " 
                    + a.local_treino_id + " where id = " 
                    + a.id + ";", conexao.getConexao());
                if (update.ExecuteNonQuery() > 0)
                {
                    conexao.encerrar();
                    return true;
                }
                else
                {
                    conexao.encerrar();
                    return false;
                }
            }
            catch (Exception exc)
            {
                conexao.encerrar();
                Console.WriteLine("Erro ao executar a query: " + exc.ToString());
                return false;
            }

        }

        public int insere(Agenda a)
        {
            try
            {
                conexao.conectar();
                MySqlCommand update = new MySqlCommand("insert into agenda VALUES (DEFAULT, '" + 
                    a.descricao + "', '" + 
                    a.tipo + "', CAST('" + 
                    a.data_inicio.ToString("yyyy-MM-dd") + "' as Date), " +
                    "CAST('" + 
                    a.hora_inicio.ToString("HH:mm") + "' as TIME), CAST('" + 
                    a.data_fim.ToString("yyyy-MM-dd") + "' as Date), " +
                    "CAST('" + 
                    a.hora_fim.ToString("HH:mm") + "' as TIME), '" + 
                    a.observacao + "', " + 
                    a.local_treino_id + ");", conexao.getConexao());
                if (update.ExecuteNonQuery() > 0)
                {
                    MySqlCommand select = new MySqlCommand("select LAST_INSERT_ID();", conexao.getConexao());
                    MySqlDataReader dr;
                    dr = select.ExecuteReader();
                    int result = -1;
                    while (dr.Read())
                    {
                        result = dr.GetInt32(0);
                    }
                    conexao.encerrar();
                    return result;
                }
                else
                {
                    conexao.encerrar();
                    return -1;
                }
            }
            catch (Exception exc)
            {
                Console.WriteLine("Erro ao executar a query: " + exc.ToString());
                conexao.encerrar();
                return -1;
            }

        }

        public bool remove(int Id)
        {
            try
            {
                conexao.conectar();
                MySqlCommand update = new MySqlCommand("delete from agenda where id = " + Id + ";", conexao.getConexao());
                if (update.ExecuteNonQuery() > 0)
                {
                    conexao.encerrar();
                    return true;
                }
                else
                {
                    conexao.encerrar();
                    return false;
                }
            }
            catch (Exception exc)
            {
                Console.WriteLine("Erro ao executar a query: " + exc.ToString());
                conexao.encerrar();
                return false;
            }

        }

        public char retornaTipoChar(int cbId)
        {
            char result = new char();
            switch (cbId)
            {
                case 0:
                    result = 'T';
                    break;
                case 1:
                    result = 'J';
                    break;
                case 2:
                    result = 'R';
                    break;
                case 3:
                    result = 'E';
                    break;
                case 4:
                    result = 'O';
                    break;
                default:
                    break;
            }
            return result;
        }
        public int retornaTipoInt(char tipo)
        {
            int result = -1;
            switch (tipo)
            {
                case 'T':
                    result = 0;
                    break;
                case 'J':
                    result = 1;
                    break;
                case 'R':
                    result = 2;
                    break;
                case 'E':
                    result = 3;
                    break;
                case 'O':
                    result = 4;
                    break;
                default:
                    break;
            }
            return result;
        }
    }
}
