﻿using GestaoFABR.Model;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoFABR.Control
{
    class ControlAgendaAtleta
    {
        private Conexao conexao = new Conexao();

        public ControlAgendaAtleta()
        {
        }

        public DataTable obter()
        {
            try
            {
                conexao.conectar();
                DataTable dt = conexao.selecionar("SELECT aa.id, aa.agenda_id, aa.pessoa_id, p.nome, a.descricao, aa.situacao, " +
                    "(CASE aa.situacao WHEN 'P' THEN 'Presente' WHEN 'F' THEN 'Faltou' WHEN 'J' THEN 'Justificou' END) AS desc_situacao, " +
                    "aa.justificativa " +
                    "FROM agenda_atleta AS aa " +
                    "left JOIN pessoa AS p ON p.id = aa.pessoa_id " +
                    "JOIN agenda AS a ON a.id = aa.agenda_id;");
                conexao.encerrar();
                return dt;

            }
            catch (Exception exc)
            {
                Console.WriteLine("Erro ao executar a query: " + exc.ToString());
                return null;
            }

        }

        public Boolean atualiza(AgendaAtleta a)
        {
            try
            {
                conexao.conectar();
                MySqlCommand update = new MySqlCommand("update agenda_atleta set agenda_id = '"
                    + a.agenda_id + "', situacao = '" + a.situacao + "', justificativa = '" + a.justificativa + "', pessoa_id = " + a.pessoa_id + " where id = " + a.id + ";", conexao.getConexao());
                if (update.ExecuteNonQuery() > 0)
                {
                    conexao.encerrar();
                    return true;
                }
                else
                {
                    conexao.encerrar();
                    return false;
                }
            }
            catch (Exception exc)
            {
                Console.WriteLine("Erro ao executar a query: " + exc.ToString());
                conexao.encerrar();
                return false;
            }

        }

        public bool insere(AgendaAtleta a)
        {
            try
            {
                conexao.conectar();
                MySqlCommand update = new MySqlCommand("insert into agenda_atleta VALUES (DEFAULT, '" + a.agenda_id + "', '" + a.situacao + "', '" + a.justificativa + "', " + a.pessoa_id + ");", conexao.getConexao());
                if (update.ExecuteNonQuery() > 0)
                {
                    conexao.encerrar();
                    return true;
                }
                else
                {
                    conexao.encerrar();
                    return false;
                }
            }
            catch (Exception exc)
            {
                conexao.encerrar();
                Console.WriteLine("Erro ao executar a query: " + exc.ToString());
                return false;
            }

        }

        public bool remove(int Id)
        {
            try
            {
                conexao.conectar();
                MySqlCommand update = new MySqlCommand("delete from agenda_atleta where id = " + Id + ";", conexao.getConexao());
                if (update.ExecuteNonQuery() > 0)
                {
                    conexao.encerrar();
                    return true;
                }
                else
                {
                    conexao.encerrar();
                    return false;
                }
            }
            catch (Exception exc)
            {
                conexao.encerrar();
                Console.WriteLine("Erro ao executar a query: " + exc.ToString());
                return false;
            }

        }
        public char retornaJustificativaChar(int cbId)
        {
            char result = new char();
            switch (cbId)
            {
                case 0:
                    result = 'p';
                    break;
                case 1:
                    result = 'F';
                    break;
                case 2:
                    result = 'J';
                    break;
                default:
                    break;
            }
            return result;
        }
        public int retornaJustificativaInt(char tipo)
        {
            int result = -1;
            switch (tipo)
            {
                case 'P':
                    result = 0;
                    break;
                case 'F':
                    result = 1;
                    break;
                case 'J':
                    result = 2;
                    break;
                default:
                    break;
            }
            return result;
        }

    }
}
