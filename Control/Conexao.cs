﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoFABR.Control
{
    class Conexao
    {
        private string strconn = "server=localhost;user id=root;password=100560;persistsecurityinfo=True;database=gestao_esportiva";
        private MySqlConnection conexao;
        public void conectar() {
            conexao = new MySqlConnection(strconn);
            conexao.Open();
        }
        
        public MySqlConnection getConexao()
        {
            return conexao;
        }
        public void encerrar()
        {
            conexao.Close();
        }

        public DataTable selecionar(string sql)
        {
            MySqlDataAdapter da = new MySqlDataAdapter(sql, this.conexao);
            DataTable dt = new DataTable();
            dt.Clear();
            da.Fill(dt);
            return dt;
        }


    }
}
