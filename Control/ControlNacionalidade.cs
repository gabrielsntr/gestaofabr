﻿using GestaoFABR.Model;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoFABR.Control
{
    class ControlNacionalidade
    {
        private Conexao conexao = new Conexao();
        public DataTable obter()
        {
            try
            {
                conexao.conectar();
                MySqlDataAdapter da = new MySqlDataAdapter("select * from nacionalidade;", conexao.getConexao());
                DataTable dt = new DataTable();
                dt.Clear();
                da.Fill(dt);
                conexao.encerrar();
                return dt;

            }
            catch (Exception exc)
            {
                Console.WriteLine("Erro ao executar a query: " + exc.ToString());
                return null;
            }

        }

        public string obterNome(int id)
        {
            try
            {
                conexao.conectar();
                MySqlCommand select = new MySqlCommand("select nome from nacionalidade where id = " + id + ";", conexao.getConexao());
                MySqlDataReader dr;
                dr = select.ExecuteReader();
                string result = "";
                while (dr.Read())
                {
                    result = dr.GetString(0);
                }
                conexao.encerrar();
                return result;

            }
            catch (Exception exc)
            {
                Console.WriteLine("Erro ao executar a query: " + exc.ToString());
                return null;
            }

        }

        public Boolean atualiza(Nacionalidade n)
        {
            try
            {
                conexao.conectar();
                MySqlCommand update = new MySqlCommand("update nacionalidade set nome = '"
                    + n.nome + "' where id = " + n.id + ";", conexao.getConexao());
                if (update.ExecuteNonQuery() > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception exc)
            {
                Console.WriteLine("Erro ao executar a query: " + exc.ToString());
                return false;
            }

        }

        public bool insere(Nacionalidade n)
        {
            try
            {
                conexao.conectar();
                MySqlCommand update = new MySqlCommand("insert into nacionalidade VALUES (DEFAULT, '"
                    + n.nome + "');", conexao.getConexao());
                if (update.ExecuteNonQuery() > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception exc)
            {
                Console.WriteLine("Erro ao executar a query: " + exc.ToString());
                return false;
            }

        }

        public bool remove(int Id)
        {
            try
            {
                conexao.conectar();
                MySqlCommand update = new MySqlCommand("delete from nacionalidade where id = " + Id + ";", conexao.getConexao());
                if (update.ExecuteNonQuery() > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception exc)
            {
                Console.WriteLine("Erro ao executar a query: " + exc.ToString());
                return false;
            }

        }

    }
}
