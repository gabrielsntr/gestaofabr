﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoFABR.Model
{
    class AgendaAtleta
    {
        public int id { get; set; }
        public int agenda_id { get; set; }
        public char situacao { get; set; }        
        public string justificativa { get; set; }
        public int pessoa_id { get; set; }

        public AgendaAtleta(int id, int agenda_id, char situacao, string justificativa, int pessoa_id)
        {
            this.id = id;
            this.agenda_id = agenda_id;
            this.situacao = situacao;
            this.justificativa = justificativa;
            this.pessoa_id = pessoa_id;
        }

        public AgendaAtleta(int agenda_id, char situacao, string justificativa, int pessoa_id)
        {
            this.agenda_id = agenda_id;
            this.situacao = situacao;
            this.justificativa = justificativa;
            this.pessoa_id = pessoa_id;
        }
    }
}
