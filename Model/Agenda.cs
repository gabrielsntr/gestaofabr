﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoFABR.Model
{
    class Agenda
    {
        public int id { get; set; }
        public string descricao { get; set; }
        public char tipo { get; set; }
        public DateTime data_inicio { get; set; }
        public DateTime hora_inicio { get; set; }
        public DateTime data_fim { get; set; }
        public DateTime hora_fim { get; set; }
        public string observacao { get; set; }
        public int local_treino_id { get; set; }

        public Agenda(int id, string descricao, char tipo, DateTime data_inicio, DateTime hora_inicio, DateTime data_fim, DateTime hora_fim, string observacao, int local_treino_id)
        {
            this.id = id;
            this.descricao = descricao;
            this.tipo = tipo;
            this.data_inicio = data_inicio;
            this.hora_inicio = hora_inicio;
            this.data_fim = data_fim;
            this.hora_fim = hora_fim;
            this.observacao = observacao;
            this.local_treino_id = local_treino_id;
        }

        public Agenda(string descricao, char tipo, DateTime data_inicio, DateTime hora_inicio, DateTime data_fim, DateTime hora_fim, string observacao, int local_treino_id)
        {
            this.descricao = descricao;
            this.tipo = tipo;
            this.data_inicio = data_inicio;
            this.hora_inicio = hora_inicio;
            this.data_fim = data_fim;
            this.hora_fim = hora_fim;
            this.observacao = observacao;
            this.local_treino_id = local_treino_id;
        }
    }
}
