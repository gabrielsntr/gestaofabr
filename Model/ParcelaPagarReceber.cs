﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoFABR.Model
{
    class ParcelaPagarReceber
    {
        public int id { get; set; }
        public int numero { get; set; }
        public float valor { get; set; }
        public float valor_recebido { get; set; }
        public DateTime data_vencimento { get; set; }
        public char situacao { get; set; }
        public int conta_pagar_receber_id { get; set; }

        public ParcelaPagarReceber(int id, int numero, float valor, float valor_recebido, DateTime data_vencimento, char situacao, int conta_pagar_receber_id)
        {
            this.id = id;
            this.numero = numero;
            this.valor = valor;
            this.valor_recebido = valor_recebido;
            this.data_vencimento = data_vencimento;
            this.situacao = situacao;
            this.conta_pagar_receber_id = conta_pagar_receber_id;
        }

        public ParcelaPagarReceber(int numero, float valor, float valor_recebido, DateTime data_vencimento, char situacao, int conta_pagar_receber_id)
        {
            this.numero = numero;
            this.valor = valor;
            this.valor_recebido = valor_recebido;
            this.data_vencimento = data_vencimento;
            this.situacao = situacao;
            this.conta_pagar_receber_id = conta_pagar_receber_id;
        }
    }
}
