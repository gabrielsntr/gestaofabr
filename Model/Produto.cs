﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoFABR.Model
{
    class Produto
    {
        public int id { get; set; }
        public string descricao { get; set; }
        public float valor { get; set; }

        public Produto(int id, string descricao, float valor)
        {
            this.id = id;
            this.descricao = descricao;
            this.valor = valor;
        }

        public Produto(string descricao, float valor)
        {
            this.descricao = descricao;
            this.valor = valor;
        }
    }
}
