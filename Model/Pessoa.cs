﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoFABR.Model
{
    class Pessoa
    {
        public int id { get; set; }
        public string nome { get; set; }
        public char tipo_pessoa { get; set; }
        public string cpf { get; set; }
        public string rg { get; set; }
        public DateTime data_nascimento { get; set; }
        public string cnpj { get; set; }
        public string insc_est { get; set; }
        public string cep { get; set; }
        public string logradouro { get; set; }
        public string numero { get; set; }
        public string bairro { get; set; }
        public int municipio_id { get; set; }
        public int isAtleta { get; set; }
        public float peso { get; set; }
        public float altura { get; set; }
        public string tam_helmet { get; set; }
        public string tam_shoulder { get; set; }
        public string tam_jersey { get; set; }
        public string tam_calca { get; set; }
        public int tam_pe { get; set; }
        public int isento_mensalidade { get; set; }
        public int ativo { get; set; }
        public int nacionalidade_id { get; set; }
        public int pessoa_id { get; set; }

        public Pessoa(int id, string nome, char tipo_pessoa, string cpf, string rg, DateTime data_nascimento, string cnpj, string insc_est, string cep, string logradouro, string numero, string bairro, int municipio_id, int isAtleta, float peso, float altura, string tam_helmet, string tam_shoulder, string tam_jersey, string tam_calca, int tam_pe, int isento_mensalidade, int ativo, int nacionalidade_id)
        {
            this.id = id;
            this.nome = nome;
            this.tipo_pessoa = tipo_pessoa;
            this.cpf = cpf;
            this.rg = rg;
            this.data_nascimento = data_nascimento;
            this.cnpj = cnpj;
            this.insc_est = insc_est;
            this.cep = cep;
            this.logradouro = logradouro;
            this.numero = numero;
            this.bairro = bairro;
            this.municipio_id = municipio_id;
            this.isAtleta = isAtleta;
            this.peso = peso;
            this.altura = altura;
            this.tam_helmet = tam_helmet;
            this.tam_shoulder = tam_shoulder;
            this.tam_jersey = tam_jersey;
            this.tam_calca = tam_calca;
            this.tam_pe = tam_pe;
            this.isento_mensalidade = isento_mensalidade;
            this.ativo = ativo;
            this.nacionalidade_id = nacionalidade_id;
        }

        public Pessoa(string nome, char tipo_pessoa, string cpf, string rg, DateTime data_nascimento, string cnpj, string insc_est, string cep, string logradouro, string numero, string bairro, int municipio_id, int isAtleta, float peso, float altura, string tam_helmet, string tam_shoulder, string tam_jersey, string tam_calca, int tam_pe, int isento_mensalidade, int ativo, int nacionalidade_id)
        {
            this.nome = nome;
            this.tipo_pessoa = tipo_pessoa;
            this.cpf = cpf;
            this.rg = rg;
            this.data_nascimento = data_nascimento;
            this.cnpj = cnpj;
            this.insc_est = insc_est;
            this.cep = cep;
            this.logradouro = logradouro;
            this.numero = numero;
            this.bairro = bairro;
            this.municipio_id = municipio_id;
            this.isAtleta = isAtleta;
            this.peso = peso;
            this.altura = altura;
            this.tam_helmet = tam_helmet;
            this.tam_shoulder = tam_shoulder;
            this.tam_jersey = tam_jersey;
            this.tam_calca = tam_calca;
            this.tam_pe = tam_pe;
            this.isento_mensalidade = isento_mensalidade;
            this.ativo = ativo;
            this.nacionalidade_id = nacionalidade_id;
            this.pessoa_id = pessoa_id;
        }
    }
}
