﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoFABR.Model
{
    class MovimentacaoFinanceira
    {
        public int id { get; set; }
        public DateTime data { get; set; }
        public char tipo { get; set; }
        public float valor { get; set; }
        public string beneficiario { get; set; }
        public string historico { get; set; }
        public int conta_bancaria_id { get; set; }
        public int parcela_pagar_receber_id { get; set; }

        public MovimentacaoFinanceira(int id, DateTime data, char tipo, float valor, string beneficiario, string historico, int conta_bancaria_id, int parcela_pagar_receber_id)
        {
            this.id = id;
            this.data = data;
            this.tipo = tipo;
            this.valor = valor;
            this.beneficiario = beneficiario;
            this.historico = historico;
            this.conta_bancaria_id = conta_bancaria_id;
            this.parcela_pagar_receber_id = parcela_pagar_receber_id;
        }

        public MovimentacaoFinanceira(DateTime data, char tipo, float valor, string beneficiario, string historico, int conta_bancaria_id, int parcela_pagar_receber_id)
        {
            this.data = data;
            this.tipo = tipo;
            this.valor = valor;
            this.beneficiario = beneficiario;
            this.historico = historico;
            this.conta_bancaria_id = conta_bancaria_id;
            this.parcela_pagar_receber_id = parcela_pagar_receber_id;
        }
    }
}
