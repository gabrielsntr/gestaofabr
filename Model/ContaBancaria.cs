﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoFABR.Model
{
    class ContaBancaria
    {
        public int id { get; set; }
        public string descricao { get; set; }
        public char tipo { get; set; }
        public string conta { get; set; }
        public string agencia { get; set; }
        public string beneficiario { get; set; }
        public ContaBancaria(int id, string descricao, char tipo, string conta, string agencia, string beneficiario)
        {
            this.id = id;
            this.descricao = descricao;
            this.tipo = tipo;
            this.conta = conta;
            this.agencia = agencia;
            this.beneficiario = beneficiario;
        }

        public ContaBancaria(string descricao, char tipo, string conta, string agencia, string beneficiario)
        {
            this.descricao = descricao;
            this.tipo = tipo;
            this.conta = conta;
            this.agencia = agencia;
            this.beneficiario = beneficiario;
        }
    }
}
