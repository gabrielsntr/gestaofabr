﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoFABR.Model
{
    class LocalTreino
    {
        public int id { get; set; }
        public string nome { get; set; }
        public string logradouro { get; set; }
        public string numero { get; set; }
        public string bairro { get; set; }

        public LocalTreino(int id, string nome, string logradouro, string numero, string bairro)
        {
            this.id = id;
            this.nome = nome;
            this.logradouro = logradouro;
            this.numero = numero;
            this.bairro = bairro;
        }
        public LocalTreino(string nome, string logradouro, string numero, string bairro)
        {
            this.id = id;
            this.nome = nome;
            this.logradouro = logradouro;
            this.numero = numero;
            this.bairro = bairro;
        }
    }
}
