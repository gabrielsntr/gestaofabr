﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoFABR.Model
{
    class Usuario
    {
        public int id { get; set; }
        public string nome { get; set; }
        public string senha { get; set; }

        public Usuario(int id, string nome, string senha)
        {
            this.id = id;
            this.nome = nome;
            this.senha = senha;
        }

        public Usuario(string nome, string senha)
        {
            this.nome = nome;
            this.senha = senha;
        }
    }
    
}
