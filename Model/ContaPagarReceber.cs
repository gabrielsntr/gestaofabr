﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoFABR.Model
{
    class ContaPagarReceber
    {
        public int id { get; set; }
        public char tipo { get; set; }
        public float valor { get; set; }
        public string historico { get; set; }
        public DateTime data_emissao { get; set; }
        public int pessoa_id { get; set; }
        public int qtd_parcelas { get; set; }

        public ContaPagarReceber(int id, char tipo, float valor, string historico, DateTime data_emissao, int pessoa_id, int qtd_parcelas)
        {
            this.id = id;
            this.tipo = tipo;
            this.valor = valor;
            this.historico = historico;
            this.data_emissao = data_emissao;
            this.pessoa_id = pessoa_id;
            this.qtd_parcelas = qtd_parcelas;
        }

        public ContaPagarReceber(char tipo, float valor, string historico, DateTime data_emissao, int pessoa_id, int qtd_parcelas)
        {
            this.tipo = tipo;
            this.valor = valor;
            this.historico = historico;
            this.data_emissao = data_emissao;
            this.pessoa_id = pessoa_id;
            this.qtd_parcelas = qtd_parcelas;
        }
    }
}
