﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoFABR.Model
{
    class MovimentacaoEstoque
    {
        public int id { get; set; }
        public DateTime data { get; set; }
        public char tipo_movimento { get; set; }
        public float quantidade { get; set; }
        public float valor { get; set; }
        public int produto_id { get; set; }

        public MovimentacaoEstoque(int id, DateTime data, char tipo_movimento, float quantidade, float valor, int produto_id)
        {
            this.id = id;
            this.data = data;
            this.tipo_movimento = tipo_movimento;
            this.quantidade = quantidade;
            this.valor = valor;
            this.produto_id = produto_id;
        }

        public MovimentacaoEstoque(DateTime data, char tipo_movimento, float quantidade, float valor, int produto_id)
        {
            this.data = data;
            this.tipo_movimento = tipo_movimento;
            this.quantidade = quantidade;
            this.valor = valor;
            this.produto_id = produto_id;
        }
    }
}
