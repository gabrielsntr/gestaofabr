﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoFABR.Model
{
    class Nacionalidade
    {
        public int id { get; set; }
        public string nome { get; set; }

        public Nacionalidade(int id, string nome)
        {
            this.id = id;
            this.nome = nome;
        }

        public Nacionalidade(string nome)
        {
            this.nome = nome;
        }
    }
}
