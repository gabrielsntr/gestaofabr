﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoFABR.Model
{
    class Municipio
    {
        public int id { get; set; }
        public string nome { get; set; }
        public string uf { get; set; }

        public Municipio(int id, string nome, string uf)
        {
            this.id = id;
            this.nome = nome;
            this.uf = uf;
        }

        public Municipio(string nome, string uf)
        {
            this.nome = nome;
            this.uf = uf;
        }
    }
}
