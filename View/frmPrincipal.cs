﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GestaoFABR.Model;
using GestaoFABR.View;

namespace GestaoFABR.View
{
    public partial class frmPrincipal : Form
    {
        public int id_Usuario;
        public frmPrincipal()
        {
            InitializeComponent();

        }

        private void FrmPrincipal_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void PresençaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmPresenca presenca = new frmPresenca();
            presenca.ShowDialog();
        }

        private void LocalDeTreinoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmLocalTreino localTreino = new frmLocalTreino();
            localTreino.ShowDialog();
        }

        private void contaBancáriaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmContaBancaria contaBancaria = new frmContaBancaria();
            contaBancaria.ShowDialog();
        }

        private void municípioToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmMunicipio municipio = new frmMunicipio();
            municipio.ShowDialog();

        }

        private void nacionalidadeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmNacionalidade nacionalidade = new frmNacionalidade();
            nacionalidade.ShowDialog();
        }

        private void PessoaToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            frmPessoaAtleta pessoa = new frmPessoaAtleta();
            pessoa.ShowDialog();

        }

        private void AgendaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmAgenda agenda = new frmAgenda();
            agenda.ShowDialog();
        }

        private void ProdutoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmProduto produto = new frmProduto();
            produto.ShowDialog();
        }
    }
}
