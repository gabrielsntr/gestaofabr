﻿using GestaoFABR.Control;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestaoFABR.View
{
    public partial class frmBusca : Form
    {
        public int id_selecionado { get; set; }
        public frmBusca(DataTable dt)
        {
            InitializeComponent();
            preencheGrid(dt);
            foreach (DataColumn coluna in (gridItems.DataSource as DataTable).Columns)
            {
                cbFiltro.Items.Add(coluna.ColumnName);
            }
            cbFiltro.SelectedIndex = 0;

        }

        public void preencheGrid(DataTable dt)
        {
            gridItems.DataSource = dt;
        }

        private void tfFiltro_TextChanged(object sender, EventArgs e)
        {
            Utils.filtraGrid(gridItems, cbFiltro, tfFiltro);
        }

        private void GridItems_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            id_selecionado = Int32.Parse(gridItems.Rows[gridItems.SelectedCells[0].RowIndex].Cells["id"].Value.ToString());
            this.Hide();
        }
    }
}
