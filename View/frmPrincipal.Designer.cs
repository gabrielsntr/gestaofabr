﻿namespace GestaoFABR.View
{
    partial class frmPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.pessoaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.agendaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.presençaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.financeiroToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.movimentaçãoFinanceiraToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.estoqueToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.movimentaçãoDeEstoqueToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cadastrosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contaBancáriaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.localDeTreinoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.municípioToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nacionalidadeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pessoaToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.produtoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.pessoaToolStripMenuItem,
            this.financeiroToolStripMenuItem,
            this.estoqueToolStripMenuItem,
            this.cadastrosToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1111, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // pessoaToolStripMenuItem
            // 
            this.pessoaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.agendaToolStripMenuItem,
            this.presençaToolStripMenuItem});
            this.pessoaToolStripMenuItem.Name = "pessoaToolStripMenuItem";
            this.pessoaToolStripMenuItem.Size = new System.Drawing.Size(60, 20);
            this.pessoaToolStripMenuItem.Text = "Agenda";
            // 
            // agendaToolStripMenuItem
            // 
            this.agendaToolStripMenuItem.Name = "agendaToolStripMenuItem";
            this.agendaToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.agendaToolStripMenuItem.Text = "Agenda";
            this.agendaToolStripMenuItem.Click += new System.EventHandler(this.AgendaToolStripMenuItem_Click);
            // 
            // presençaToolStripMenuItem
            // 
            this.presençaToolStripMenuItem.Name = "presençaToolStripMenuItem";
            this.presençaToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.presençaToolStripMenuItem.Text = "Presenças";
            this.presençaToolStripMenuItem.Click += new System.EventHandler(this.PresençaToolStripMenuItem_Click);
            // 
            // financeiroToolStripMenuItem
            // 
            this.financeiroToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.contasToolStripMenuItem,
            this.movimentaçãoFinanceiraToolStripMenuItem});
            this.financeiroToolStripMenuItem.Enabled = false;
            this.financeiroToolStripMenuItem.Name = "financeiroToolStripMenuItem";
            this.financeiroToolStripMenuItem.Size = new System.Drawing.Size(74, 20);
            this.financeiroToolStripMenuItem.Text = "Financeiro";
            // 
            // contasToolStripMenuItem
            // 
            this.contasToolStripMenuItem.Name = "contasToolStripMenuItem";
            this.contasToolStripMenuItem.Size = new System.Drawing.Size(211, 22);
            this.contasToolStripMenuItem.Text = "Contas a Pagar/Receber";
            // 
            // movimentaçãoFinanceiraToolStripMenuItem
            // 
            this.movimentaçãoFinanceiraToolStripMenuItem.Name = "movimentaçãoFinanceiraToolStripMenuItem";
            this.movimentaçãoFinanceiraToolStripMenuItem.Size = new System.Drawing.Size(211, 22);
            this.movimentaçãoFinanceiraToolStripMenuItem.Text = "Movimentação Financeira";
            // 
            // estoqueToolStripMenuItem
            // 
            this.estoqueToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.movimentaçãoDeEstoqueToolStripMenuItem});
            this.estoqueToolStripMenuItem.Enabled = false;
            this.estoqueToolStripMenuItem.Name = "estoqueToolStripMenuItem";
            this.estoqueToolStripMenuItem.Size = new System.Drawing.Size(61, 20);
            this.estoqueToolStripMenuItem.Text = "Estoque";
            // 
            // movimentaçãoDeEstoqueToolStripMenuItem
            // 
            this.movimentaçãoDeEstoqueToolStripMenuItem.Name = "movimentaçãoDeEstoqueToolStripMenuItem";
            this.movimentaçãoDeEstoqueToolStripMenuItem.Size = new System.Drawing.Size(215, 22);
            this.movimentaçãoDeEstoqueToolStripMenuItem.Text = "Movimentação de Estoque";
            // 
            // cadastrosToolStripMenuItem
            // 
            this.cadastrosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.contaBancáriaToolStripMenuItem,
            this.localDeTreinoToolStripMenuItem,
            this.municípioToolStripMenuItem,
            this.nacionalidadeToolStripMenuItem,
            this.pessoaToolStripMenuItem1,
            this.produtoToolStripMenuItem});
            this.cadastrosToolStripMenuItem.Name = "cadastrosToolStripMenuItem";
            this.cadastrosToolStripMenuItem.Size = new System.Drawing.Size(71, 20);
            this.cadastrosToolStripMenuItem.Text = "Cadastros";
            // 
            // contaBancáriaToolStripMenuItem
            // 
            this.contaBancáriaToolStripMenuItem.Name = "contaBancáriaToolStripMenuItem";
            this.contaBancáriaToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.contaBancáriaToolStripMenuItem.Text = "Conta Bancária";
            this.contaBancáriaToolStripMenuItem.Click += new System.EventHandler(this.contaBancáriaToolStripMenuItem_Click);
            // 
            // localDeTreinoToolStripMenuItem
            // 
            this.localDeTreinoToolStripMenuItem.Name = "localDeTreinoToolStripMenuItem";
            this.localDeTreinoToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.localDeTreinoToolStripMenuItem.Text = "Local de Treino";
            this.localDeTreinoToolStripMenuItem.Click += new System.EventHandler(this.LocalDeTreinoToolStripMenuItem_Click);
            // 
            // municípioToolStripMenuItem
            // 
            this.municípioToolStripMenuItem.Name = "municípioToolStripMenuItem";
            this.municípioToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.municípioToolStripMenuItem.Text = "Município";
            this.municípioToolStripMenuItem.Click += new System.EventHandler(this.municípioToolStripMenuItem_Click);
            // 
            // nacionalidadeToolStripMenuItem
            // 
            this.nacionalidadeToolStripMenuItem.Name = "nacionalidadeToolStripMenuItem";
            this.nacionalidadeToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.nacionalidadeToolStripMenuItem.Text = "Nacionalidade";
            this.nacionalidadeToolStripMenuItem.Click += new System.EventHandler(this.nacionalidadeToolStripMenuItem_Click);
            // 
            // pessoaToolStripMenuItem1
            // 
            this.pessoaToolStripMenuItem1.Name = "pessoaToolStripMenuItem1";
            this.pessoaToolStripMenuItem1.Size = new System.Drawing.Size(180, 22);
            this.pessoaToolStripMenuItem1.Text = "Pessoa/Atleta";
            this.pessoaToolStripMenuItem1.Click += new System.EventHandler(this.PessoaToolStripMenuItem1_Click);
            // 
            // produtoToolStripMenuItem
            // 
            this.produtoToolStripMenuItem.Name = "produtoToolStripMenuItem";
            this.produtoToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.produtoToolStripMenuItem.Text = "Produto";
            this.produtoToolStripMenuItem.Click += new System.EventHandler(this.ProdutoToolStripMenuItem_Click);
            // 
            // frmPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1111, 606);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "frmPrincipal";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Gestão FABR";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FrmPrincipal_FormClosed);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem pessoaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem agendaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem presençaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem financeiroToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem contasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem movimentaçãoFinanceiraToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem estoqueToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem movimentaçãoDeEstoqueToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cadastrosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pessoaToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem produtoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem municípioToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nacionalidadeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem localDeTreinoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem contaBancáriaToolStripMenuItem;
    }
}