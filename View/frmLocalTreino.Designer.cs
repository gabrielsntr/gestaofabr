﻿namespace GestaoFABR.View
{
    partial class frmLocalTreino
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.local_treinoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gestao_esportivaDataSet = new GestaoFABR.gestao_esportivaDataSet();
            this.local_treinoTableAdapter = new GestaoFABR.gestao_esportivaDataSetTableAdapters.local_treinoTableAdapter();
            this.tableAdapterManager = new GestaoFABR.gestao_esportivaDataSetTableAdapters.TableAdapterManager();
            this.localtreinoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridItems = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.tfID = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tfNome = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tfLogradouro = new System.Windows.Forms.TextBox();
            this.tfNumero = new System.Windows.Forms.TextBox();
            this.tfBairro = new System.Windows.Forms.TextBox();
            this.lblNumero = new System.Windows.Forms.Label();
            this.lblBairro = new System.Windows.Forms.Label();
            this.btnInserir = new System.Windows.Forms.Button();
            this.btnAlterar = new System.Windows.Forms.Button();
            this.btnExcluir = new System.Windows.Forms.Button();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.btnSalvar = new System.Windows.Forms.Button();
            this.cbFiltro = new System.Windows.Forms.ComboBox();
            this.tfFiltro = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.local_treinoBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gestao_esportivaDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.localtreinoBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridItems)).BeginInit();
            this.SuspendLayout();
            // 
            // local_treinoBindingSource
            // 
            this.local_treinoBindingSource.DataMember = "local_treino";
            this.local_treinoBindingSource.DataSource = this.gestao_esportivaDataSet;
            // 
            // gestao_esportivaDataSet
            // 
            this.gestao_esportivaDataSet.DataSetName = "gestao_esportivaDataSet";
            this.gestao_esportivaDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // local_treinoTableAdapter
            // 
            this.local_treinoTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.Connection = null;
            this.tableAdapterManager.UpdateOrder = GestaoFABR.gestao_esportivaDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // localtreinoBindingSource
            // 
            this.localtreinoBindingSource.DataMember = "local_treino";
            this.localtreinoBindingSource.DataSource = this.gestao_esportivaDataSet;
            // 
            // gridItems
            // 
            this.gridItems.AllowUserToAddRows = false;
            this.gridItems.AllowUserToDeleteRows = false;
            this.gridItems.AllowUserToOrderColumns = true;
            this.gridItems.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridItems.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridItems.Location = new System.Drawing.Point(12, 280);
            this.gridItems.Name = "gridItems";
            this.gridItems.ReadOnly = true;
            this.gridItems.Size = new System.Drawing.Size(660, 271);
            this.gridItems.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 69);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(19, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Id:";
            // 
            // tfID
            // 
            this.tfID.Location = new System.Drawing.Point(82, 66);
            this.tfID.Name = "tfID";
            this.tfID.ReadOnly = true;
            this.tfID.Size = new System.Drawing.Size(60, 20);
            this.tfID.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 95);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Nome:";
            // 
            // tfNome
            // 
            this.tfNome.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tfNome.Location = new System.Drawing.Point(82, 92);
            this.tfNome.Name = "tfNome";
            this.tfNome.ReadOnly = true;
            this.tfNome.Size = new System.Drawing.Size(590, 20);
            this.tfNome.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 121);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(64, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Logradouro:";
            // 
            // tfLogradouro
            // 
            this.tfLogradouro.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tfLogradouro.Location = new System.Drawing.Point(82, 118);
            this.tfLogradouro.Name = "tfLogradouro";
            this.tfLogradouro.ReadOnly = true;
            this.tfLogradouro.Size = new System.Drawing.Size(590, 20);
            this.tfLogradouro.TabIndex = 7;
            // 
            // tfNumero
            // 
            this.tfNumero.Location = new System.Drawing.Point(82, 144);
            this.tfNumero.Name = "tfNumero";
            this.tfNumero.ReadOnly = true;
            this.tfNumero.Size = new System.Drawing.Size(60, 20);
            this.tfNumero.TabIndex = 8;
            // 
            // tfBairro
            // 
            this.tfBairro.Location = new System.Drawing.Point(82, 170);
            this.tfBairro.Name = "tfBairro";
            this.tfBairro.ReadOnly = true;
            this.tfBairro.Size = new System.Drawing.Size(217, 20);
            this.tfBairro.TabIndex = 9;
            // 
            // lblNumero
            // 
            this.lblNumero.AutoSize = true;
            this.lblNumero.Location = new System.Drawing.Point(12, 147);
            this.lblNumero.Name = "lblNumero";
            this.lblNumero.Size = new System.Drawing.Size(47, 13);
            this.lblNumero.TabIndex = 10;
            this.lblNumero.Text = "Número:";
            // 
            // lblBairro
            // 
            this.lblBairro.AutoSize = true;
            this.lblBairro.Location = new System.Drawing.Point(12, 173);
            this.lblBairro.Name = "lblBairro";
            this.lblBairro.Size = new System.Drawing.Size(37, 13);
            this.lblBairro.TabIndex = 11;
            this.lblBairro.Text = "Bairro:";
            // 
            // btnInserir
            // 
            this.btnInserir.Location = new System.Drawing.Point(224, 12);
            this.btnInserir.Name = "btnInserir";
            this.btnInserir.Size = new System.Drawing.Size(75, 23);
            this.btnInserir.TabIndex = 12;
            this.btnInserir.Text = "Inserir";
            this.btnInserir.UseVisualStyleBackColor = true;
            this.btnInserir.Click += new System.EventHandler(this.btnInserir_Click);
            // 
            // btnAlterar
            // 
            this.btnAlterar.Location = new System.Drawing.Point(305, 12);
            this.btnAlterar.Name = "btnAlterar";
            this.btnAlterar.Size = new System.Drawing.Size(75, 23);
            this.btnAlterar.TabIndex = 13;
            this.btnAlterar.Text = "Alterar";
            this.btnAlterar.UseVisualStyleBackColor = true;
            this.btnAlterar.Click += new System.EventHandler(this.BtnAlterar_Click);
            // 
            // btnExcluir
            // 
            this.btnExcluir.Location = new System.Drawing.Point(386, 12);
            this.btnExcluir.Name = "btnExcluir";
            this.btnExcluir.Size = new System.Drawing.Size(75, 23);
            this.btnExcluir.TabIndex = 14;
            this.btnExcluir.Text = "Excluir";
            this.btnExcluir.UseVisualStyleBackColor = true;
            this.btnExcluir.Click += new System.EventHandler(this.btnExcluir_Click);
            // 
            // btnCancelar
            // 
            this.btnCancelar.Location = new System.Drawing.Point(345, 209);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(75, 23);
            this.btnCancelar.TabIndex = 16;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.BtnCancelar_Click);
            // 
            // btnSalvar
            // 
            this.btnSalvar.Location = new System.Drawing.Point(264, 209);
            this.btnSalvar.Name = "btnSalvar";
            this.btnSalvar.Size = new System.Drawing.Size(75, 23);
            this.btnSalvar.TabIndex = 15;
            this.btnSalvar.Text = "Salvar";
            this.btnSalvar.UseVisualStyleBackColor = true;
            this.btnSalvar.Click += new System.EventHandler(this.BtnSalvar_Click);
            // 
            // cbFiltro
            // 
            this.cbFiltro.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cbFiltro.FormattingEnabled = true;
            this.cbFiltro.Location = new System.Drawing.Point(12, 253);
            this.cbFiltro.Name = "cbFiltro";
            this.cbFiltro.Size = new System.Drawing.Size(112, 21);
            this.cbFiltro.TabIndex = 17;
            // 
            // tfFiltro
            // 
            this.tfFiltro.Location = new System.Drawing.Point(131, 253);
            this.tfFiltro.Name = "tfFiltro";
            this.tfFiltro.Size = new System.Drawing.Size(541, 20);
            this.tfFiltro.TabIndex = 18;
            this.tfFiltro.TextChanged += new System.EventHandler(this.TfFiltro_TextChanged);
            // 
            // frmLocalTreino
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(684, 561);
            this.Controls.Add(this.tfFiltro);
            this.Controls.Add(this.cbFiltro);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.btnSalvar);
            this.Controls.Add(this.btnExcluir);
            this.Controls.Add(this.btnAlterar);
            this.Controls.Add(this.btnInserir);
            this.Controls.Add(this.lblBairro);
            this.Controls.Add(this.lblNumero);
            this.Controls.Add(this.tfBairro);
            this.Controls.Add(this.tfNumero);
            this.Controls.Add(this.tfLogradouro);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.tfNome);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tfID);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.gridItems);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "frmLocalTreino";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Cadastro de Local de Treino";
            ((System.ComponentModel.ISupportInitialize)(this.local_treinoBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gestao_esportivaDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.localtreinoBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridItems)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private gestao_esportivaDataSet gestao_esportivaDataSet;
        private System.Windows.Forms.BindingSource local_treinoBindingSource;
        private gestao_esportivaDataSetTableAdapters.local_treinoTableAdapter local_treinoTableAdapter;
        private gestao_esportivaDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.BindingSource localtreinoBindingSource;
        private System.Windows.Forms.DataGridView gridItems;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tfID;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tfNome;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tfLogradouro;
        private System.Windows.Forms.TextBox tfNumero;
        private System.Windows.Forms.TextBox tfBairro;
        private System.Windows.Forms.Label lblNumero;
        private System.Windows.Forms.Label lblBairro;
        private System.Windows.Forms.Button btnInserir;
        private System.Windows.Forms.Button btnAlterar;
        private System.Windows.Forms.Button btnExcluir;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Button btnSalvar;
        private System.Windows.Forms.ComboBox cbFiltro;
        private System.Windows.Forms.TextBox tfFiltro;
    }
}