﻿using GestaoFABR.Control;
using GestaoFABR.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestaoFABR.View
{
    public partial class frmNacionalidade : Form
    {
        private bool isAtualizaCadastro = false;
        private ControlNacionalidade control = new ControlNacionalidade();


        public void desabilitaTudo()
        {
            tfID.Text = "";
            tfNome.Text = "";
            tfID.ReadOnly = true;
            tfNome.ReadOnly = true;
            btnAlterar.Enabled = false;
            btnCancelar.Enabled = false;
            btnExcluir.Enabled = false;
            btnInserir.Enabled = false;
            btnSalvar.Enabled = false;
        }

        public void estadoInicial()
        {
            desabilitaTudo();
            preencheGrid();
            btnInserir.Enabled = true;
            btnAlterar.Enabled = true;
            btnExcluir.Enabled = true;
        }

        public void preencheGrid()
        {
            gridItems.DataSource = control.obter();
        }

        public frmNacionalidade()
        {
            InitializeComponent();
            estadoInicial();
            foreach (DataColumn coluna in (gridItems.DataSource as DataTable).Columns)
            {
                cbFiltro.Items.Add(coluna.ColumnName);
            }
            cbFiltro.SelectedIndex = 0;
        }

        private void btnInserir_Click(object sender, EventArgs e)
        {
            isAtualizaCadastro = false;
            desabilitaTudo();
            btnCancelar.Enabled = true;
            btnSalvar.Enabled = true;
            tfNome.ReadOnly = false;
            tfNome.Focus();
        }

        private void btnAlterar_Click(object sender, EventArgs e)
        {
            if (gridItems.SelectedCells.Count > 0)
            {
                isAtualizaCadastro = true;
                desabilitaTudo();
                btnCancelar.Enabled = true;
                btnSalvar.Enabled = true;
                tfNome.ReadOnly = false;
                tfID.Text = gridItems.Rows[gridItems.SelectedCells[0].RowIndex].Cells["id"].Value.ToString();
                tfNome.Text = gridItems.Rows[gridItems.SelectedCells[0].RowIndex].Cells["nome"].Value.ToString();
                tfNome.Focus();
            }
            else
            {
                MessageBox.Show("Por favor, selecione um registro.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnExcluir_Click(object sender, EventArgs e)
        {
            if (gridItems.SelectedCells.Count > 0)
            {
                if (MessageBox.Show("Tem certeza que deseja excluir o registro?", "Aviso", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    bool result = control.remove(Int32.Parse(gridItems.Rows[gridItems.SelectedCells[0].RowIndex].Cells["Id"].Value.ToString()));
                    if (!result)
                    {
                        MessageBox.Show("Ocorreu um erro ao excluir o registro.", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                    {
                        estadoInicial();
                    }

                }
            }
            else
            {
                MessageBox.Show("Por favor, selecione um registro.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            if (isAtualizaCadastro)
            {
                bool result = control.atualiza(new Nacionalidade(Int32.Parse(tfID.Text), tfNome.Text));
                if (!result)
                {
                    MessageBox.Show("Ocorreu um erro ao alterar o registro.", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    estadoInicial();
                }
            }
            else
            {
                bool result = control.insere(new Nacionalidade(tfNome.Text));
                if (!result)
                {
                    MessageBox.Show("Ocorreu um erro ao inserir o registro.", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    estadoInicial();
                }
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            estadoInicial();
        }

        private void tfFiltro_TextChanged(object sender, EventArgs e)
        {
            Utils.filtraGrid(gridItems, cbFiltro, tfFiltro);
        }
    }
}
