﻿using GestaoFABR.Control;
using GestaoFABR.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestaoFABR.View
{
    public partial class frmMunicipio : Form
    {
        private bool isAtualizaCadastro = false;
        private ControlMunicipio control = new ControlMunicipio();


        public void desabilitaTudo()
        {
            tfID.Text = "";
            tfNome.Text = "";
            cbUF.SelectedIndex = -1;
            tfID.ReadOnly = true;
            tfNome.ReadOnly = true;
            cbUF.Enabled = false;
            btnAlterar.Enabled = false;
            btnCancelar.Enabled = false;
            btnExcluir.Enabled = false;
            btnInserir.Enabled = false;
            btnSalvar.Enabled = false;
        }

        public void estadoInicial()
        {
            desabilitaTudo();
            btnInserir.Enabled = true;
            btnAlterar.Enabled = true;
            btnExcluir.Enabled = true;
        }

        public void preencheGrid()
        {
            gridItems.DataSource = control.obter();
        }

        public frmMunicipio()
        {
            InitializeComponent();
            cbUF.Items.AddRange(new string[] { "AC", "AL", "AM", "AP", "BA", "CE", "DF", "ES", "GO", "MA", "MG", "MS", "MT", "PA", "PB", "PE", "PI", "PR", "RJ", "RN", "RO", "RR", "RS", "SC", "SE", "SP", "TO" });
            estadoInicial();
            preencheGrid();
            foreach (DataColumn coluna in (gridItems.DataSource as DataTable).Columns)
            {
                cbFiltro.Items.Add(coluna.ColumnName);
            }
            cbFiltro.SelectedIndex = 0;

        }

        private void btnInserir_Click(object sender, EventArgs e)
        {
            isAtualizaCadastro = false;
            desabilitaTudo();
            btnCancelar.Enabled = true;
            btnSalvar.Enabled = true;
            tfNome.ReadOnly = false;
            cbUF.Enabled = true;
            cbUF.SelectedIndex = 0;
            tfNome.Focus();
        }

        private void btnAlterar_Click(object sender, EventArgs e)
        {
            if (gridItems.SelectedCells.Count > 0)
            {
                isAtualizaCadastro = true;
                desabilitaTudo();
                btnCancelar.Enabled = true;
                btnSalvar.Enabled = true;
                tfNome.ReadOnly = false;
                cbUF.Enabled = true;
                tfID.Text = gridItems.Rows[gridItems.SelectedCells[0].RowIndex].Cells["id"].Value.ToString();
                tfNome.Text = gridItems.Rows[gridItems.SelectedCells[0].RowIndex].Cells["nome"].Value.ToString();
                cbUF.SelectedItem = gridItems.Rows[gridItems.SelectedCells[0].RowIndex].Cells["uf"].Value.ToString();
                tfNome.Focus();
            }
            else
            {
                MessageBox.Show("Por favor, selecione um registro.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            if (isAtualizaCadastro)
            {
                bool result = control.atualiza(new Municipio(Int32.Parse(tfID.Text), tfNome.Text, cbUF.SelectedItem.ToString()));
                if (!result)
                {
                    MessageBox.Show("Ocorreu um erro ao alterar o registro.", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    estadoInicial();
                    preencheGrid();
                }
            }
            else
            {
                bool result = control.insere(new Municipio(tfNome.Text, cbUF.SelectedItem.ToString()));
                if (!result)
                {
                    MessageBox.Show("Ocorreu um erro ao inserir o registro.", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    estadoInicial();
                    preencheGrid();
                }
            }
        }

        private void btnExcluir_Click(object sender, EventArgs e)
        {
            if (gridItems.SelectedCells.Count > 0)
            {
                if (MessageBox.Show("Tem certeza que deseja excluir o registro?", "Aviso", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    bool result = control.remove(Int32.Parse(gridItems.Rows[gridItems.SelectedCells[0].RowIndex].Cells["Id"].Value.ToString()));
                    if (!result)
                    {
                        MessageBox.Show("Ocorreu um erro ao excluir o registro.", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                    {
                        estadoInicial();
                        preencheGrid();
                    }

                }
            }
            else
            {
                MessageBox.Show("Por favor, selecione um registro.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

        }


        private void btnCancelar_Click(object sender, EventArgs e)
        {
            estadoInicial();
        }

        private void tfFiltro_TextChanged(object sender, EventArgs e)
        {
            Utils.filtraGrid(gridItems, cbFiltro, tfFiltro);
        }
    }
}
