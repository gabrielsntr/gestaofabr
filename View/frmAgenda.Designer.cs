﻿namespace GestaoFABR.View
{
    partial class frmAgenda
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnExcluir = new System.Windows.Forms.Button();
            this.btnAlterar = new System.Windows.Forms.Button();
            this.btnInserir = new System.Windows.Forms.Button();
            this.tfFiltro = new System.Windows.Forms.TextBox();
            this.cbFiltro = new System.Windows.Forms.ComboBox();
            this.gridItems = new System.Windows.Forms.DataGridView();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.btnSalvar = new System.Windows.Forms.Button();
            this.lblDescricao = new System.Windows.Forms.Label();
            this.tfDescricao = new System.Windows.Forms.TextBox();
            this.tfID = new System.Windows.Forms.TextBox();
            this.lblId = new System.Windows.Forms.Label();
            this.cbTipo = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.dtDataInicio = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.dtHoraInicio = new System.Windows.Forms.DateTimePicker();
            this.dtHoraFim = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.dtDataFim = new System.Windows.Forms.DateTimePicker();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.tfObservacao = new System.Windows.Forms.TextBox();
            this.tfNomeLocal = new System.Windows.Forms.TextBox();
            this.btnBuscarLocal = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.tfIdLocal = new System.Windows.Forms.TextBox();
            this.btnParticipantes = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.gridItems)).BeginInit();
            this.SuspendLayout();
            // 
            // btnExcluir
            // 
            this.btnExcluir.Location = new System.Drawing.Point(291, 12);
            this.btnExcluir.Name = "btnExcluir";
            this.btnExcluir.Size = new System.Drawing.Size(75, 23);
            this.btnExcluir.TabIndex = 53;
            this.btnExcluir.Text = "Excluir";
            this.btnExcluir.UseVisualStyleBackColor = true;
            this.btnExcluir.Click += new System.EventHandler(this.btnExcluir_Click);
            // 
            // btnAlterar
            // 
            this.btnAlterar.Location = new System.Drawing.Point(210, 12);
            this.btnAlterar.Name = "btnAlterar";
            this.btnAlterar.Size = new System.Drawing.Size(75, 23);
            this.btnAlterar.TabIndex = 52;
            this.btnAlterar.Text = "Alterar";
            this.btnAlterar.UseVisualStyleBackColor = true;
            this.btnAlterar.Click += new System.EventHandler(this.btnAlterar_Click);
            // 
            // btnInserir
            // 
            this.btnInserir.Location = new System.Drawing.Point(129, 12);
            this.btnInserir.Name = "btnInserir";
            this.btnInserir.Size = new System.Drawing.Size(75, 23);
            this.btnInserir.TabIndex = 51;
            this.btnInserir.Text = "Inserir";
            this.btnInserir.UseVisualStyleBackColor = true;
            this.btnInserir.Click += new System.EventHandler(this.btnInserir_Click);
            // 
            // tfFiltro
            // 
            this.tfFiltro.Location = new System.Drawing.Point(130, 276);
            this.tfFiltro.Name = "tfFiltro";
            this.tfFiltro.Size = new System.Drawing.Size(442, 20);
            this.tfFiltro.TabIndex = 56;
            this.tfFiltro.TextChanged += new System.EventHandler(this.tfFiltro_TextChanged);
            // 
            // cbFiltro
            // 
            this.cbFiltro.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cbFiltro.FormattingEnabled = true;
            this.cbFiltro.Location = new System.Drawing.Point(12, 276);
            this.cbFiltro.Name = "cbFiltro";
            this.cbFiltro.Size = new System.Drawing.Size(112, 21);
            this.cbFiltro.TabIndex = 55;
            // 
            // gridItems
            // 
            this.gridItems.AllowUserToAddRows = false;
            this.gridItems.AllowUserToDeleteRows = false;
            this.gridItems.AllowUserToOrderColumns = true;
            this.gridItems.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridItems.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridItems.Location = new System.Drawing.Point(12, 302);
            this.gridItems.Name = "gridItems";
            this.gridItems.ReadOnly = true;
            this.gridItems.Size = new System.Drawing.Size(560, 197);
            this.gridItems.TabIndex = 54;
            // 
            // btnCancelar
            // 
            this.btnCancelar.Location = new System.Drawing.Point(295, 247);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(75, 23);
            this.btnCancelar.TabIndex = 58;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // btnSalvar
            // 
            this.btnSalvar.Location = new System.Drawing.Point(214, 247);
            this.btnSalvar.Name = "btnSalvar";
            this.btnSalvar.Size = new System.Drawing.Size(75, 23);
            this.btnSalvar.TabIndex = 57;
            this.btnSalvar.Text = "Salvar";
            this.btnSalvar.UseVisualStyleBackColor = true;
            this.btnSalvar.Click += new System.EventHandler(this.btnSalvar_Click);
            // 
            // lblDescricao
            // 
            this.lblDescricao.AutoSize = true;
            this.lblDescricao.Location = new System.Drawing.Point(12, 81);
            this.lblDescricao.Name = "lblDescricao";
            this.lblDescricao.Size = new System.Drawing.Size(58, 13);
            this.lblDescricao.TabIndex = 62;
            this.lblDescricao.Text = "Descrição:";
            // 
            // tfDescricao
            // 
            this.tfDescricao.Location = new System.Drawing.Point(85, 78);
            this.tfDescricao.Name = "tfDescricao";
            this.tfDescricao.ReadOnly = true;
            this.tfDescricao.Size = new System.Drawing.Size(487, 20);
            this.tfDescricao.TabIndex = 59;
            // 
            // tfID
            // 
            this.tfID.Location = new System.Drawing.Point(85, 52);
            this.tfID.Name = "tfID";
            this.tfID.ReadOnly = true;
            this.tfID.Size = new System.Drawing.Size(91, 20);
            this.tfID.TabIndex = 61;
            // 
            // lblId
            // 
            this.lblId.AutoSize = true;
            this.lblId.Location = new System.Drawing.Point(12, 55);
            this.lblId.Name = "lblId";
            this.lblId.Size = new System.Drawing.Size(19, 13);
            this.lblId.TabIndex = 60;
            this.lblId.Text = "Id:";
            // 
            // cbTipo
            // 
            this.cbTipo.Enabled = false;
            this.cbTipo.FormattingEnabled = true;
            this.cbTipo.Items.AddRange(new object[] {
            "Treino",
            "Jogo",
            "Reunião",
            "Evento",
            "Outro"});
            this.cbTipo.Location = new System.Drawing.Point(85, 104);
            this.cbTipo.Name = "cbTipo";
            this.cbTipo.Size = new System.Drawing.Size(135, 21);
            this.cbTipo.TabIndex = 63;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 107);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(31, 13);
            this.label1.TabIndex = 64;
            this.label1.Text = "Tipo:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 135);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 13);
            this.label2.TabIndex = 65;
            this.label2.Text = "Data Início:";
            // 
            // dtDataInicio
            // 
            this.dtDataInicio.Enabled = false;
            this.dtDataInicio.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtDataInicio.Location = new System.Drawing.Point(85, 131);
            this.dtDataInicio.Name = "dtDataInicio";
            this.dtDataInicio.Size = new System.Drawing.Size(107, 20);
            this.dtDataInicio.TabIndex = 66;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(210, 135);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(63, 13);
            this.label3.TabIndex = 67;
            this.label3.Text = "Hora Início:";
            // 
            // dtHoraInicio
            // 
            this.dtHoraInicio.Enabled = false;
            this.dtHoraInicio.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtHoraInicio.Location = new System.Drawing.Point(279, 131);
            this.dtHoraInicio.Name = "dtHoraInicio";
            this.dtHoraInicio.Size = new System.Drawing.Size(82, 20);
            this.dtHoraInicio.TabIndex = 68;
            // 
            // dtHoraFim
            // 
            this.dtHoraFim.Enabled = false;
            this.dtHoraFim.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtHoraFim.Location = new System.Drawing.Point(279, 157);
            this.dtHoraFim.Name = "dtHoraFim";
            this.dtHoraFim.Size = new System.Drawing.Size(82, 20);
            this.dtHoraFim.TabIndex = 72;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(210, 161);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(52, 13);
            this.label4.TabIndex = 71;
            this.label4.Text = "Hora Fim:";
            // 
            // dtDataFim
            // 
            this.dtDataFim.Enabled = false;
            this.dtDataFim.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtDataFim.Location = new System.Drawing.Point(85, 157);
            this.dtDataFim.Name = "dtDataFim";
            this.dtDataFim.Size = new System.Drawing.Size(107, 20);
            this.dtDataFim.TabIndex = 70;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 161);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(52, 13);
            this.label5.TabIndex = 69;
            this.label5.Text = "Data Fim:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 213);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(68, 13);
            this.label6.TabIndex = 73;
            this.label6.Text = "Observação:";
            // 
            // tfObservacao
            // 
            this.tfObservacao.Location = new System.Drawing.Point(85, 210);
            this.tfObservacao.Name = "tfObservacao";
            this.tfObservacao.ReadOnly = true;
            this.tfObservacao.Size = new System.Drawing.Size(487, 20);
            this.tfObservacao.TabIndex = 74;
            // 
            // tfNomeLocal
            // 
            this.tfNomeLocal.Location = new System.Drawing.Point(236, 183);
            this.tfNomeLocal.Name = "tfNomeLocal";
            this.tfNomeLocal.ReadOnly = true;
            this.tfNomeLocal.Size = new System.Drawing.Size(336, 20);
            this.tfNomeLocal.TabIndex = 78;
            // 
            // btnBuscarLocal
            // 
            this.btnBuscarLocal.Location = new System.Drawing.Point(164, 181);
            this.btnBuscarLocal.Name = "btnBuscarLocal";
            this.btnBuscarLocal.Size = new System.Drawing.Size(66, 23);
            this.btnBuscarLocal.TabIndex = 77;
            this.btnBuscarLocal.Text = "Buscar";
            this.btnBuscarLocal.UseVisualStyleBackColor = true;
            this.btnBuscarLocal.Click += new System.EventHandler(this.btnBuscarLocal_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(12, 186);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(69, 13);
            this.label8.TabIndex = 79;
            this.label8.Text = "Local Treino:";
            // 
            // tfIdLocal
            // 
            this.tfIdLocal.Location = new System.Drawing.Point(85, 183);
            this.tfIdLocal.Name = "tfIdLocal";
            this.tfIdLocal.ReadOnly = true;
            this.tfIdLocal.Size = new System.Drawing.Size(73, 20);
            this.tfIdLocal.TabIndex = 76;
            this.tfIdLocal.Leave += new System.EventHandler(this.tfIdLocal_Leave);
            // 
            // btnParticipantes
            // 
            this.btnParticipantes.Location = new System.Drawing.Point(372, 12);
            this.btnParticipantes.Name = "btnParticipantes";
            this.btnParticipantes.Size = new System.Drawing.Size(84, 23);
            this.btnParticipantes.TabIndex = 80;
            this.btnParticipantes.Text = "Participantes";
            this.btnParticipantes.UseVisualStyleBackColor = true;
            this.btnParticipantes.Click += new System.EventHandler(this.btnParticipantes_Click);
            // 
            // frmAgenda
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(584, 511);
            this.Controls.Add(this.btnParticipantes);
            this.Controls.Add(this.tfNomeLocal);
            this.Controls.Add(this.btnBuscarLocal);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.tfIdLocal);
            this.Controls.Add(this.tfObservacao);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.dtHoraFim);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.dtDataFim);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.dtHoraInicio);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.dtDataInicio);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cbTipo);
            this.Controls.Add(this.lblDescricao);
            this.Controls.Add(this.tfDescricao);
            this.Controls.Add(this.tfID);
            this.Controls.Add(this.lblId);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.btnSalvar);
            this.Controls.Add(this.tfFiltro);
            this.Controls.Add(this.cbFiltro);
            this.Controls.Add(this.gridItems);
            this.Controls.Add(this.btnExcluir);
            this.Controls.Add(this.btnAlterar);
            this.Controls.Add(this.btnInserir);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "frmAgenda";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Agenda";
            ((System.ComponentModel.ISupportInitialize)(this.gridItems)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnExcluir;
        private System.Windows.Forms.Button btnAlterar;
        private System.Windows.Forms.Button btnInserir;
        private System.Windows.Forms.TextBox tfFiltro;
        private System.Windows.Forms.ComboBox cbFiltro;
        private System.Windows.Forms.DataGridView gridItems;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Button btnSalvar;
        private System.Windows.Forms.Label lblDescricao;
        private System.Windows.Forms.TextBox tfDescricao;
        private System.Windows.Forms.TextBox tfID;
        private System.Windows.Forms.Label lblId;
        private System.Windows.Forms.ComboBox cbTipo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dtDataInicio;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker dtHoraInicio;
        private System.Windows.Forms.DateTimePicker dtHoraFim;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DateTimePicker dtDataFim;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tfObservacao;
        private System.Windows.Forms.TextBox tfNomeLocal;
        private System.Windows.Forms.Button btnBuscarLocal;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox tfIdLocal;
        private System.Windows.Forms.Button btnParticipantes;
    }
}