﻿using GestaoFABR.Control;
using GestaoFABR.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestaoFABR.View
{
    public partial class frmAgenda : Form
    {
        private bool isAtualizaCadastro = false;
        private ControlAgenda controlAgenda = new ControlAgenda();
        private ControlPessoa controlPessoa = new ControlPessoa();
        private ControlLocalTreino controlLocalTreino = new ControlLocalTreino();
        private ControlAgendaAtleta controlAgendaAtleta = new ControlAgendaAtleta();
        public void desabilitaTudo()
        {
            //Limpa
            tfID.Text = "";
            tfDescricao.Text = "";
            cbTipo.SelectedIndex = -1;
            dtDataInicio.Value = DateTime.Today;
            dtHoraInicio.Value = DateTime.Now;
            dtDataFim.Value = DateTime.Today;
            dtHoraFim.Value = DateTime.Now;
            tfIdLocal.Text = "";
            tfNomeLocal.Text = "";
            tfObservacao.Text = "";

            //Desabilita
            tfID.ReadOnly = true;
            tfDescricao.ReadOnly = true;
            cbTipo.Enabled = false;
            dtDataInicio.Enabled = false;
            dtHoraInicio.Enabled = false;
            dtDataFim.Enabled = false;
            dtHoraFim.Enabled = false;
            tfIdLocal.ReadOnly = true;
            tfNomeLocal.ReadOnly = true;
            tfObservacao.ReadOnly = true;
            btnAlterar.Enabled = false;
            btnCancelar.Enabled = false;
            btnExcluir.Enabled = false;
            btnInserir.Enabled = false;
            btnSalvar.Enabled = false;
            btnParticipantes.Enabled = false;
            btnBuscarLocal.Enabled = false;
        }

        public void habilitaTudo()
        {
            tfID.ReadOnly = true;

            tfDescricao.ReadOnly = false;

            cbTipo.Enabled = true;
            dtDataInicio.Enabled = true;
            dtHoraInicio.Enabled = true;
            dtDataFim.Enabled = true;
            dtHoraFim.Enabled = true;

            tfIdLocal.ReadOnly = false;
            tfObservacao.ReadOnly = false;

            btnCancelar.Enabled = true;
            btnSalvar.Enabled = true;
            btnParticipantes.Enabled = true;
            btnBuscarLocal.Enabled = true;
        }

        public void estadoInicial()
        {
            desabilitaTudo();
            preencheGrid();
            btnInserir.Enabled = true;
            btnAlterar.Enabled = true;
            btnExcluir.Enabled = true;
        }

        public void preencheGrid()
        {
            gridItems.DataSource = controlAgenda.obter();
        }


        public frmAgenda()
        {
            InitializeComponent();
            estadoInicial();
            dtHoraInicio.CustomFormat = "HH:mm";
            dtHoraFim.CustomFormat = "HH:mm";

        }

        private void btnInserir_Click(object sender, EventArgs e)
        {
            isAtualizaCadastro = false;
            habilitaTudo();
        }

        private void btnAlterar_Click(object sender, EventArgs e)
        {
            if (gridItems.SelectedCells.Count > 0)
            {
                isAtualizaCadastro = true;
                habilitaTudo();
                btnParticipantes.Enabled = true;
                tfID.Text = gridItems.Rows[gridItems.SelectedCells[0].RowIndex].Cells["id"].Value.ToString();
                tfDescricao.Text = gridItems.Rows[gridItems.SelectedCells[0].RowIndex].Cells["descricao"].Value.ToString();
                cbTipo.SelectedIndex = controlAgenda.retornaTipoInt(char.Parse(gridItems.Rows[gridItems.SelectedCells[0].RowIndex].Cells["tipo"].Value.ToString()));
                dtDataInicio.Value = Utils.stringToDtPicker(gridItems.Rows[gridItems.SelectedCells[0].RowIndex].Cells["data_inicio"].Value.ToString());
                dtHoraInicio.Value = Utils.stringToDtPickerHora(gridItems.Rows[gridItems.SelectedCells[0].RowIndex].Cells["hora_inicio"].Value.ToString());
                dtDataFim.Value = Utils.stringToDtPicker(gridItems.Rows[gridItems.SelectedCells[0].RowIndex].Cells["data_fim"].Value.ToString());
                dtHoraFim.Value = Utils.stringToDtPickerHora(gridItems.Rows[gridItems.SelectedCells[0].RowIndex].Cells["hora_fim"].Value.ToString());
                tfIdLocal.Text = gridItems.Rows[gridItems.SelectedCells[0].RowIndex].Cells["local_treino_id"].Value.ToString();
                tfNomeLocal.Text = gridItems.Rows[gridItems.SelectedCells[0].RowIndex].Cells["nome"].Value.ToString();
                tfObservacao.Text = gridItems.Rows[gridItems.SelectedCells[0].RowIndex].Cells["observacao"].Value.ToString();
            }
            else
            {
                MessageBox.Show("Por favor, selecione um registro.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnExcluir_Click(object sender, EventArgs e)
        {
            if (gridItems.SelectedCells.Count > 0)
            {
                if (MessageBox.Show("Tem certeza que deseja excluir o registro?", "Aviso", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    bool result = controlAgenda.remove(Int32.Parse(gridItems.Rows[gridItems.SelectedCells[0].RowIndex].Cells["Id"].Value.ToString()));
                    if (!result)
                    {
                        MessageBox.Show("Ocorreu um erro ao excluir o registro.", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                    {
                        estadoInicial();
                    }

                }
            }
            else
            {
                MessageBox.Show("Por favor, selecione um registro.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

        }

        private void btnParticipantes_Click(object sender, EventArgs e)
        {
            frmPresenca presenca = new frmPresenca();
            
            presenca.ShowDialog();
            presenca.id_agenda = Int32.Parse(tfID.Text);
        }

        private void btnBuscarLocal_Click(object sender, EventArgs e)
        {
            frmBusca busca = new frmBusca(controlLocalTreino.obter());
            busca.Text = busca.Text + "Local de Treino";
            busca.ShowDialog();
            tfIdLocal.Text = busca.id_selecionado.ToString();
            tfNomeLocal.Text = controlLocalTreino.obterNome(Int32.Parse(tfIdLocal.Text));
            busca = null;
            
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            if (Utils.verificaDataHora(dtDataInicio.Value, dtDataFim.Value) && Utils.verificaDataHora(dtHoraInicio.Value, dtHoraFim.Value))
            {
                int id_local = 0;
                if (!String.IsNullOrWhiteSpace(tfIdLocal.Text))
                {
                    id_local = Int32.Parse(tfIdLocal.Text);
                }
                if (isAtualizaCadastro)
                {
                    bool result = controlAgenda.atualiza(new Agenda(Int32.Parse(tfID.Text), tfDescricao.Text, controlAgenda.retornaTipoChar(cbTipo.SelectedIndex), dtDataInicio.Value, dtHoraInicio.Value, dtDataFim.Value, dtHoraFim.Value, tfObservacao.Text, id_local));
                    if (!result)
                    {
                        MessageBox.Show("Ocorreu um erro ao alterar o registro.", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                    {
                        estadoInicial();
                    }
                }
                else
                {
                    int result = controlAgenda.insere(new Agenda(tfDescricao.Text, controlAgenda.retornaTipoChar(cbTipo.SelectedIndex), dtDataInicio.Value, dtHoraInicio.Value, dtDataFim.Value, dtHoraFim.Value, tfObservacao.Text, id_local));
                    if (result < 1)
                    {
                        MessageBox.Show("Ocorreu um erro ao inserir o registro.", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                    {
                        if (controlAgenda.retornaTipoChar(cbTipo.SelectedIndex) == 'J' || controlAgenda.retornaTipoChar(cbTipo.SelectedIndex) == 'T')
                        {
                            foreach (int atleta in controlPessoa.obterAtletas())
                            {
                                controlAgendaAtleta.insere(new AgendaAtleta(result, 'F', "", atleta));
                            }
                        }
                        estadoInicial();
                    }
                }
            } else
            {
                MessageBox.Show("A Data e Hora Final deve ser maior que a Data e Hora Inicial.", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            estadoInicial();
        }

        private void tfFiltro_TextChanged(object sender, EventArgs e)
        {
            Utils.filtraGrid(gridItems, cbFiltro, tfFiltro);
        }

        private void tfIdLocal_Leave(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(tfIdLocal.Text))
            {
                tfNomeLocal.Text = controlLocalTreino.obterNome(Int32.Parse(tfIdLocal.Text));
            }
        }
    }
}
