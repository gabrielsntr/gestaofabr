﻿using GestaoFABR.Control;
using GestaoFABR.View;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestaoFABR.Model
{
    public partial class frmPresenca : Form
    {
        private bool isAtualizaCadastro = false;
        public int id_agenda = 0;
        
        private ControlAgendaAtleta controlAgendaAtleta = new ControlAgendaAtleta();
        private ControlAgenda controlAgenda = new ControlAgenda();
        private ControlPessoa controlPessoa = new ControlPessoa();
        public void habilitaTudo()
        {
            tfIDAgenda.ReadOnly = false;
            tfNomeAgenda.ReadOnly = true;
            cbSituacao.Enabled = true;
            tfJustificativa.ReadOnly = false;
            tfIdPessoa.ReadOnly = false;

            btnCancelar.Enabled = true;
            btnSalvar.Enabled = true;
            btnIdPessoa.Enabled = true;
            btnAgenda.Enabled = true;
        }

        public void desabilitaTudo()
        {
            tfIDAgenda.Text = "";
            tfId.Text = "";
            tfNomeAgenda.Text = "";
            cbSituacao.SelectedIndex = -1;
            tfJustificativa.Text = "";
            tfIdPessoa.Text = "";
            tfNomePessoa.Text = "";

            tfIDAgenda.ReadOnly = true;
            tfId.ReadOnly = true;
            tfNomeAgenda.ReadOnly = true;
            cbSituacao.Enabled = false;
            tfJustificativa.ReadOnly = true;
            tfIdPessoa.ReadOnly = true;            

            btnAlterar.Enabled = false;
            btnCancelar.Enabled = false;
            btnExcluir.Enabled = false;
            btnInserir.Enabled = false;
            btnSalvar.Enabled = false;
            btnIdPessoa.Enabled = false;
            btnAgenda.Enabled = false;
        }

        public void estadoInicial()
        {
            desabilitaTudo();
            preencheGrid();
            btnInserir.Enabled = true;
            btnAlterar.Enabled = true;
            btnExcluir.Enabled = true;
        }

        public void preencheGrid()
        {
            gridItems.DataSource = controlAgendaAtleta.obter();
        }


        public frmPresenca()
        {
            InitializeComponent();
            estadoInicial();
            foreach (DataColumn coluna in (gridItems.DataSource as DataTable).Columns)
            {
                cbFiltro.Items.Add(coluna.ColumnName);
            }
            cbFiltro.SelectedIndex = 0;
        }


        private void btnInserir_Click(object sender, EventArgs e)
        {
            desabilitaTudo();
            Console.WriteLine(id_agenda.ToString());
            if (id_agenda > 0)
            {
                tfIDAgenda.Text = id_agenda.ToString();
                cbSituacao.Focus();

            } else
            {
                tfIDAgenda.ReadOnly = false;
                btnAgenda.Enabled = true;
                tfIDAgenda.Focus();
            }
            isAtualizaCadastro = false;
            btnCancelar.Enabled = true;
            btnSalvar.Enabled = true;


        }

        private void btnAlterar_Click(object sender, EventArgs e)
        {
            if (gridItems.SelectedCells.Count > 0)
            {
                isAtualizaCadastro = true;
                desabilitaTudo();
                habilitaTudo();
                btnCancelar.Enabled = true;
                btnSalvar.Enabled = true;
                tfId.Text = gridItems.Rows[gridItems.SelectedCells[0].RowIndex].Cells["id"].Value.ToString();
                tfIDAgenda.Text = gridItems.Rows[gridItems.SelectedCells[0].RowIndex].Cells["agenda_id"].Value.ToString();
                cbSituacao.SelectedIndex = controlAgendaAtleta.retornaJustificativaInt(char.Parse(gridItems.Rows[gridItems.SelectedCells[0].RowIndex].Cells["situacao"].Value.ToString()));
                tfJustificativa.Text = gridItems.Rows[gridItems.SelectedCells[0].RowIndex].Cells["justificativa"].Value.ToString();
                tfIdPessoa.Text = gridItems.Rows[gridItems.SelectedCells[0].RowIndex].Cells["pessoa_id"].Value.ToString();
                tfNomePessoa.Text = gridItems.Rows[gridItems.SelectedCells[0].RowIndex].Cells["nome"].Value.ToString();
                cbSituacao.Focus();
            }
            else
            {
                MessageBox.Show("Por favor, selecione um registro.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnExcluir_Click(object sender, EventArgs e)
        {
            if (gridItems.SelectedCells.Count > 0)
            {
                if (MessageBox.Show("Tem certeza que deseja excluir o registro?", "Aviso", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    bool result = controlAgendaAtleta.remove(Int32.Parse(gridItems.Rows[gridItems.SelectedCells[0].RowIndex].Cells["Id"].Value.ToString()));
                    if (!result)
                    {
                        MessageBox.Show("Ocorreu um erro ao excluir o registro.", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                    {
                        estadoInicial();
                    }

                }
            }
            else
            {
                MessageBox.Show("Por favor, selecione um registro.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnAgenda_Click(object sender, EventArgs e)
        {
            frmBusca busca = new frmBusca(controlAgenda.obter());
            busca.Text = busca.Text + "Agenda";
            busca.ShowDialog();
            tfIDAgenda.Text = busca.id_selecionado.ToString();
            if (!string.IsNullOrWhiteSpace(tfIDAgenda.Text))
            {
                tfNomeAgenda.Text = controlAgenda.obterNome(Int32.Parse(tfIDAgenda.Text));
                habilitaTudo();
            }
            busca = null;
        }

        private void btnIdPessoa_Click(object sender, EventArgs e)
        {

        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            if (isAtualizaCadastro)
            {
                bool result = controlAgendaAtleta.atualiza(new AgendaAtleta(Int32.Parse(tfId.Text), Int32.Parse(tfIDAgenda.Text), controlAgendaAtleta.retornaJustificativaChar(cbSituacao.SelectedIndex), tfJustificativa.Text, Int32.Parse(tfIdPessoa.Text)));
                if (!result)
                {
                    MessageBox.Show("Ocorreu um erro ao alterar o registro.", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    estadoInicial();
                }
            }
            else
            {
                bool result = controlAgendaAtleta.insere(new AgendaAtleta(Int32.Parse(tfIDAgenda.Text), controlAgendaAtleta.retornaJustificativaChar(cbSituacao.SelectedIndex), tfJustificativa.Text, Int32.Parse(tfIdPessoa.Text)));
                if (!result)
                {
                    MessageBox.Show("Ocorreu um erro ao inserir o registro.", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    estadoInicial();
                }
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            estadoInicial();
        }

        private void tfIDAgenda_Leave(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(tfIDAgenda.Text))
            {
                tfNomeAgenda.Text = controlAgenda.obterNome(Int32.Parse(tfIDAgenda.Text));
                habilitaTudo();
            }
        }

        private void TfFiltro_TextChanged(object sender, EventArgs e)
        {
            Utils.filtraGrid(gridItems, cbFiltro, tfFiltro);
        }
    }
}
