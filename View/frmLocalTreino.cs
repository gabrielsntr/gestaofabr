﻿using GestaoFABR.Control;
using GestaoFABR.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestaoFABR.View
{
    public partial class frmLocalTreino : Form
    {
        private bool isAtualizaCadastro = false;
        private ControlLocalTreino control = new ControlLocalTreino();
        
        
        public void desabilitaTudo()
        {
            tfID.Text = "";
            tfLogradouro.Text = "";
            tfNome.Text = "";
            tfNumero.Text = "";
            tfBairro.Text = "";
            tfFiltro.Text = "";
            tfID.ReadOnly = true;
            tfLogradouro.ReadOnly = true;
            tfNome.ReadOnly = true;
            tfNumero.ReadOnly = true;
            tfBairro.ReadOnly = true;
            btnAlterar.Enabled = false;
            btnCancelar.Enabled = false;
            btnExcluir.Enabled = false;
            btnInserir.Enabled = false;
            btnSalvar.Enabled = false;
        }

        public void estadoInicial()
        {
            desabilitaTudo();
            btnInserir.Enabled = true;
            btnAlterar.Enabled = true;
            btnExcluir.Enabled = true;
        }

        public void preencheGrid()
        {
            gridItems.DataSource = control.obter();      
        }
        public frmLocalTreino()
        {
            InitializeComponent();
            estadoInicial();
            preencheGrid();
            foreach (DataColumn coluna in (gridItems.DataSource as DataTable).Columns)
            {
                cbFiltro.Items.Add(coluna.ColumnName);
            }
            cbFiltro.SelectedIndex = 0;

        }
        
        private void FrmLocalTreino_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'gestao_esportivaDataSet.local_treino' table. You can move, or remove it, as needed.
            this.local_treinoTableAdapter.Fill(this.gestao_esportivaDataSet.local_treino);

        }


        private void DataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }


        private void BtnAlterar_Click(object sender, EventArgs e)
        {
            if (gridItems.SelectedCells.Count > 0)
            {
                isAtualizaCadastro = true;
                desabilitaTudo();
                btnCancelar.Enabled = true;
                btnSalvar.Enabled = true;
                tfLogradouro.ReadOnly = false;
                tfNome.ReadOnly = false;
                tfNumero.ReadOnly = false;
                tfBairro.ReadOnly = false;
                tfID.Text = gridItems.Rows[gridItems.SelectedCells[0].RowIndex].Cells["id"].Value.ToString();
                tfNome.Text = gridItems.Rows[gridItems.SelectedCells[0].RowIndex].Cells["nome"].Value.ToString();
                tfLogradouro.Text = gridItems.Rows[gridItems.SelectedCells[0].RowIndex].Cells["logradouro"].Value.ToString();
                tfNumero.Text = gridItems.Rows[gridItems.SelectedCells[0].RowIndex].Cells["numero"].Value.ToString();
                tfBairro.Text = gridItems.Rows[gridItems.SelectedCells[0].RowIndex].Cells["bairro"].Value.ToString();
                tfNome.Focus();
            } else
            {
                MessageBox.Show("Por favor, selecione um registro.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void BtnCancelar_Click(object sender, EventArgs e)
        {
            estadoInicial();
        }

        private void BtnSalvar_Click(object sender, EventArgs e)
        {
            if (isAtualizaCadastro)
            {
               bool result = control.atualiza(new LocalTreino(Int32.Parse(tfID.Text), tfNome.Text, tfLogradouro.Text, tfNumero.Text, tfBairro.Text));
               if (!result)
               {
                  MessageBox.Show("Ocorreu um erro ao alterar o registro.", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
               } else
               {
                    estadoInicial();
                    preencheGrid();
               }
            } else
            {
                bool result = control.insere(new LocalTreino(tfNome.Text, tfLogradouro.Text, tfNumero.Text, tfBairro.Text));
                if (!result)
                {
                    MessageBox.Show("Ocorreu um erro ao inserir o registro.", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    estadoInicial();
                    preencheGrid();
                }
            }
        }

        private void btnInserir_Click(object sender, EventArgs e)
        {
            isAtualizaCadastro = false;
            desabilitaTudo();
            btnCancelar.Enabled = true;
            btnSalvar.Enabled = true;
            tfLogradouro.ReadOnly = false;
            tfNome.ReadOnly = false;
            tfNumero.ReadOnly = false;
            tfBairro.ReadOnly = false;
            tfNome.Focus();

        }

        private void btnExcluir_Click(object sender, EventArgs e)
        {
            if (gridItems.SelectedCells.Count > 0)
            {
                if (MessageBox.Show("Tem certeza que deseja excluir o registro?", "Aviso", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    bool result = control.remove(Int32.Parse(gridItems.Rows[gridItems.SelectedCells[0].RowIndex].Cells["Id"].Value.ToString()));
                    if (!result)
                    {
                        MessageBox.Show("Ocorreu um erro ao excluir o registro.", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                    {
                        estadoInicial();
                        preencheGrid();
                    }

                }
            } else
            {
                MessageBox.Show("Por favor, selecione um registro.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void TfFiltro_TextChanged(object sender, EventArgs e)
        {
            Utils.filtraGrid(gridItems, cbFiltro, tfFiltro);
        }
    }
}
