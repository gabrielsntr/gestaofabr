﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql;
using MySql.Data.MySqlClient;
using GestaoFABR.Control;


namespace GestaoFABR
{
    public partial class frmLogin : Form
    {
        private int idUsuario;
        public frmLogin()
        {
            InitializeComponent();
            tfUsuario.Focus();
            tfUsuario.Text = "admin";
            tfSenha.Text = "admin";
            btnEntrar.Focus();
        }

        private void BtnCancelar_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void BtnEntrar_Click(object sender, EventArgs e)
        {

            Conexao conn = new Conexao();
            conn.conectar();
            MySqlCommand select = new MySqlCommand("select * from usuario where nome = '" + tfUsuario.Text + "' and senha = '" + tfSenha.Text + "';", conn.getConexao());            
            MySqlDataReader reader;
            try
            {
                conn.conectar();
                reader = select.ExecuteReader();
                int i = 0;
                while (reader.Read())
                {
                    i++;
                    idUsuario = reader.GetInt32(0);
                }
                if (i == 0)
                {
                    lblErro.Visible = true;
                    tfUsuario.Focus();
                }
                else
                {
                    GestaoFABR.View.frmPrincipal principal = new GestaoFABR.View.frmPrincipal();
                    principal.id_Usuario = this.idUsuario;
                    this.Hide();
                    principal.Show();
                    
                }
            } catch (Exception exc)
            {
                MessageBox.Show("Erro ao conectar-se ao banco. Mensagem: " + exc.ToString(), "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            } finally
            {
                conn.encerrar();
            }
        }

        private void Label3_Click(object sender, EventArgs e)
        {

        }
    }
}
