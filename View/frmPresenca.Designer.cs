﻿namespace GestaoFABR.Model
{
    partial class frmPresenca
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnExcluir = new System.Windows.Forms.Button();
            this.btnAlterar = new System.Windows.Forms.Button();
            this.btnInserir = new System.Windows.Forms.Button();
            this.tfIDAgenda = new System.Windows.Forms.TextBox();
            this.lblId = new System.Windows.Forms.Label();
            this.tfNomeAgenda = new System.Windows.Forms.TextBox();
            this.btnAgenda = new System.Windows.Forms.Button();
            this.tfId = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.cbSituacao = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tfJustificativa = new System.Windows.Forms.TextBox();
            this.tfNomePessoa = new System.Windows.Forms.TextBox();
            this.btnIdPessoa = new System.Windows.Forms.Button();
            this.tfIdPessoa = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tfFiltro = new System.Windows.Forms.TextBox();
            this.cbFiltro = new System.Windows.Forms.ComboBox();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.btnSalvar = new System.Windows.Forms.Button();
            this.gridItems = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.gridItems)).BeginInit();
            this.SuspendLayout();
            // 
            // btnExcluir
            // 
            this.btnExcluir.Location = new System.Drawing.Point(311, 12);
            this.btnExcluir.Name = "btnExcluir";
            this.btnExcluir.Size = new System.Drawing.Size(75, 23);
            this.btnExcluir.TabIndex = 56;
            this.btnExcluir.Text = "Excluir";
            this.btnExcluir.UseVisualStyleBackColor = true;
            this.btnExcluir.Click += new System.EventHandler(this.btnExcluir_Click);
            // 
            // btnAlterar
            // 
            this.btnAlterar.Location = new System.Drawing.Point(230, 12);
            this.btnAlterar.Name = "btnAlterar";
            this.btnAlterar.Size = new System.Drawing.Size(75, 23);
            this.btnAlterar.TabIndex = 55;
            this.btnAlterar.Text = "Alterar";
            this.btnAlterar.UseVisualStyleBackColor = true;
            this.btnAlterar.Click += new System.EventHandler(this.btnAlterar_Click);
            // 
            // btnInserir
            // 
            this.btnInserir.Location = new System.Drawing.Point(149, 12);
            this.btnInserir.Name = "btnInserir";
            this.btnInserir.Size = new System.Drawing.Size(75, 23);
            this.btnInserir.TabIndex = 54;
            this.btnInserir.Text = "Inserir";
            this.btnInserir.UseVisualStyleBackColor = true;
            this.btnInserir.Click += new System.EventHandler(this.btnInserir_Click);
            // 
            // tfIDAgenda
            // 
            this.tfIDAgenda.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tfIDAgenda.Location = new System.Drawing.Point(82, 52);
            this.tfIDAgenda.Name = "tfIDAgenda";
            this.tfIDAgenda.ReadOnly = true;
            this.tfIDAgenda.Size = new System.Drawing.Size(60, 20);
            this.tfIDAgenda.TabIndex = 58;
            this.tfIDAgenda.Leave += new System.EventHandler(this.tfIDAgenda_Leave);
            // 
            // lblId
            // 
            this.lblId.AutoSize = true;
            this.lblId.Location = new System.Drawing.Point(12, 55);
            this.lblId.Name = "lblId";
            this.lblId.Size = new System.Drawing.Size(59, 13);
            this.lblId.TabIndex = 57;
            this.lblId.Text = "Id Agenda:";
            // 
            // tfNomeAgenda
            // 
            this.tfNomeAgenda.Location = new System.Drawing.Point(220, 52);
            this.tfNomeAgenda.Name = "tfNomeAgenda";
            this.tfNomeAgenda.ReadOnly = true;
            this.tfNomeAgenda.Size = new System.Drawing.Size(302, 20);
            this.tfNomeAgenda.TabIndex = 60;
            // 
            // btnAgenda
            // 
            this.btnAgenda.Location = new System.Drawing.Point(148, 50);
            this.btnAgenda.Name = "btnAgenda";
            this.btnAgenda.Size = new System.Drawing.Size(66, 23);
            this.btnAgenda.TabIndex = 59;
            this.btnAgenda.Text = "Buscar";
            this.btnAgenda.UseVisualStyleBackColor = true;
            this.btnAgenda.Click += new System.EventHandler(this.btnAgenda_Click);
            // 
            // tfId
            // 
            this.tfId.Location = new System.Drawing.Point(82, 78);
            this.tfId.Name = "tfId";
            this.tfId.ReadOnly = true;
            this.tfId.Size = new System.Drawing.Size(60, 20);
            this.tfId.TabIndex = 62;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 81);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(19, 13);
            this.label1.TabIndex = 61;
            this.label1.Text = "Id:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(12, 107);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(52, 13);
            this.label10.TabIndex = 64;
            this.label10.Text = "Situação:";
            // 
            // cbSituacao
            // 
            this.cbSituacao.Enabled = false;
            this.cbSituacao.FormattingEnabled = true;
            this.cbSituacao.Items.AddRange(new object[] {
            "Presente",
            "Faltou",
            "Justificou"});
            this.cbSituacao.Location = new System.Drawing.Point(82, 104);
            this.cbSituacao.Name = "cbSituacao";
            this.cbSituacao.Size = new System.Drawing.Size(135, 21);
            this.cbSituacao.TabIndex = 63;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 134);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(65, 13);
            this.label5.TabIndex = 66;
            this.label5.Text = "Justificativa:";
            // 
            // tfJustificativa
            // 
            this.tfJustificativa.Location = new System.Drawing.Point(82, 131);
            this.tfJustificativa.Name = "tfJustificativa";
            this.tfJustificativa.ReadOnly = true;
            this.tfJustificativa.Size = new System.Drawing.Size(440, 20);
            this.tfJustificativa.TabIndex = 65;
            // 
            // tfNomePessoa
            // 
            this.tfNomePessoa.Location = new System.Drawing.Point(220, 157);
            this.tfNomePessoa.Name = "tfNomePessoa";
            this.tfNomePessoa.ReadOnly = true;
            this.tfNomePessoa.Size = new System.Drawing.Size(302, 20);
            this.tfNomePessoa.TabIndex = 70;
            // 
            // btnIdPessoa
            // 
            this.btnIdPessoa.Location = new System.Drawing.Point(148, 155);
            this.btnIdPessoa.Name = "btnIdPessoa";
            this.btnIdPessoa.Size = new System.Drawing.Size(66, 23);
            this.btnIdPessoa.TabIndex = 69;
            this.btnIdPessoa.Text = "Buscar";
            this.btnIdPessoa.UseVisualStyleBackColor = true;
            this.btnIdPessoa.Click += new System.EventHandler(this.btnIdPessoa_Click);
            // 
            // tfIdPessoa
            // 
            this.tfIdPessoa.Location = new System.Drawing.Point(82, 157);
            this.tfIdPessoa.Name = "tfIdPessoa";
            this.tfIdPessoa.ReadOnly = true;
            this.tfIdPessoa.Size = new System.Drawing.Size(60, 20);
            this.tfIdPessoa.TabIndex = 68;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 160);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 13);
            this.label2.TabIndex = 67;
            this.label2.Text = "Id Pessoa:";
            // 
            // tfFiltro
            // 
            this.tfFiltro.Location = new System.Drawing.Point(134, 219);
            this.tfFiltro.Name = "tfFiltro";
            this.tfFiltro.Size = new System.Drawing.Size(388, 20);
            this.tfFiltro.TabIndex = 75;
            this.tfFiltro.TextChanged += new System.EventHandler(this.TfFiltro_TextChanged);
            // 
            // cbFiltro
            // 
            this.cbFiltro.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cbFiltro.FormattingEnabled = true;
            this.cbFiltro.Location = new System.Drawing.Point(15, 219);
            this.cbFiltro.Name = "cbFiltro";
            this.cbFiltro.Size = new System.Drawing.Size(112, 21);
            this.cbFiltro.TabIndex = 74;
            // 
            // btnCancelar
            // 
            this.btnCancelar.Location = new System.Drawing.Point(270, 190);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(75, 23);
            this.btnCancelar.TabIndex = 72;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // btnSalvar
            // 
            this.btnSalvar.Location = new System.Drawing.Point(189, 190);
            this.btnSalvar.Name = "btnSalvar";
            this.btnSalvar.Size = new System.Drawing.Size(75, 23);
            this.btnSalvar.TabIndex = 71;
            this.btnSalvar.Text = "Salvar";
            this.btnSalvar.UseVisualStyleBackColor = true;
            this.btnSalvar.Click += new System.EventHandler(this.btnSalvar_Click);
            // 
            // gridItems
            // 
            this.gridItems.AllowUserToAddRows = false;
            this.gridItems.AllowUserToDeleteRows = false;
            this.gridItems.AllowUserToOrderColumns = true;
            this.gridItems.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridItems.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridItems.Location = new System.Drawing.Point(15, 245);
            this.gridItems.Name = "gridItems";
            this.gridItems.ReadOnly = true;
            this.gridItems.Size = new System.Drawing.Size(507, 254);
            this.gridItems.TabIndex = 73;
            // 
            // frmPresenca
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(534, 511);
            this.Controls.Add(this.tfFiltro);
            this.Controls.Add(this.cbFiltro);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.btnSalvar);
            this.Controls.Add(this.gridItems);
            this.Controls.Add(this.tfNomePessoa);
            this.Controls.Add(this.btnIdPessoa);
            this.Controls.Add(this.tfIdPessoa);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.tfJustificativa);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.cbSituacao);
            this.Controls.Add(this.tfId);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tfNomeAgenda);
            this.Controls.Add(this.btnAgenda);
            this.Controls.Add(this.tfIDAgenda);
            this.Controls.Add(this.lblId);
            this.Controls.Add(this.btnExcluir);
            this.Controls.Add(this.btnAlterar);
            this.Controls.Add(this.btnInserir);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "frmPresenca";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Cadastro de Presenças";
            ((System.ComponentModel.ISupportInitialize)(this.gridItems)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnExcluir;
        private System.Windows.Forms.Button btnAlterar;
        private System.Windows.Forms.Button btnInserir;
        private System.Windows.Forms.Label lblId;
        private System.Windows.Forms.TextBox tfNomeAgenda;
        private System.Windows.Forms.Button btnAgenda;
        private System.Windows.Forms.TextBox tfId;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox cbSituacao;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tfJustificativa;
        private System.Windows.Forms.TextBox tfNomePessoa;
        private System.Windows.Forms.Button btnIdPessoa;
        private System.Windows.Forms.TextBox tfIdPessoa;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tfFiltro;
        private System.Windows.Forms.ComboBox cbFiltro;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Button btnSalvar;
        private System.Windows.Forms.DataGridView gridItems;
        public System.Windows.Forms.TextBox tfIDAgenda;
    }
}