﻿namespace GestaoFABR.View
{
    partial class frmPessoaAtleta
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnExcluir = new System.Windows.Forms.Button();
            this.btnAlterar = new System.Windows.Forms.Button();
            this.btnInserir = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPessoa = new System.Windows.Forms.TabPage();
            this.lblDtNasc = new System.Windows.Forms.Label();
            this.dtNasc = new System.Windows.Forms.DateTimePicker();
            this.cbAtleta = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.cbTipoPessoa = new System.Windows.Forms.ComboBox();
            this.tfInscEst = new System.Windows.Forms.MaskedTextBox();
            this.lblInscEst = new System.Windows.Forms.Label();
            this.tfCNPJ = new System.Windows.Forms.MaskedTextBox();
            this.lblCNPJ = new System.Windows.Forms.Label();
            this.tfNomeMunicipio = new System.Windows.Forms.TextBox();
            this.btnBuscarMunicipio = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.tfIdMunicipio = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.tfBairro = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tfNumero = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tfLogradouro = new System.Windows.Forms.TextBox();
            this.tfCEP = new System.Windows.Forms.MaskedTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tfRG = new System.Windows.Forms.MaskedTextBox();
            this.lblRG = new System.Windows.Forms.Label();
            this.tfCPF = new System.Windows.Forms.MaskedTextBox();
            this.lblCPF = new System.Windows.Forms.Label();
            this.lblDescricao = new System.Windows.Forms.Label();
            this.tfNome = new System.Windows.Forms.TextBox();
            this.tfID = new System.Windows.Forms.TextBox();
            this.lblId = new System.Windows.Forms.Label();
            this.tabAtleta = new System.Windows.Forms.TabPage();
            this.tfNomeNacionalidade = new System.Windows.Forms.TextBox();
            this.btnBuscarNacionalidade = new System.Windows.Forms.Button();
            this.tfIdNacionalidade = new System.Windows.Forms.TextBox();
            this.cbAtivo = new System.Windows.Forms.ComboBox();
            this.label17 = new System.Windows.Forms.Label();
            this.cbIsentoMens = new System.Windows.Forms.ComboBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.numCalcado = new System.Windows.Forms.NumericUpDown();
            this.cbTamCalca = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.cbTamJersey = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.cbTamShoulder = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.cbTamHelmet = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.tfAltura = new System.Windows.Forms.MaskedTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tfPeso = new System.Windows.Forms.MaskedTextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.tfFiltro = new System.Windows.Forms.TextBox();
            this.cbFiltro = new System.Windows.Forms.ComboBox();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.btnSalvar = new System.Windows.Forms.Button();
            this.gridItems = new System.Windows.Forms.DataGridView();
            this.tabControl1.SuspendLayout();
            this.tabPessoa.SuspendLayout();
            this.tabAtleta.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numCalcado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridItems)).BeginInit();
            this.SuspendLayout();
            // 
            // btnExcluir
            // 
            this.btnExcluir.Location = new System.Drawing.Point(386, 12);
            this.btnExcluir.Name = "btnExcluir";
            this.btnExcluir.Size = new System.Drawing.Size(75, 23);
            this.btnExcluir.TabIndex = 2;
            this.btnExcluir.Text = "Excluir";
            this.btnExcluir.UseVisualStyleBackColor = true;
            this.btnExcluir.Click += new System.EventHandler(this.BtnExcluir_Click);
            // 
            // btnAlterar
            // 
            this.btnAlterar.Location = new System.Drawing.Point(305, 12);
            this.btnAlterar.Name = "btnAlterar";
            this.btnAlterar.Size = new System.Drawing.Size(75, 23);
            this.btnAlterar.TabIndex = 1;
            this.btnAlterar.Text = "Alterar";
            this.btnAlterar.UseVisualStyleBackColor = true;
            this.btnAlterar.Click += new System.EventHandler(this.BtnAlterar_Click);
            // 
            // btnInserir
            // 
            this.btnInserir.Location = new System.Drawing.Point(224, 12);
            this.btnInserir.Name = "btnInserir";
            this.btnInserir.Size = new System.Drawing.Size(75, 23);
            this.btnInserir.TabIndex = 0;
            this.btnInserir.Text = "Inserir";
            this.btnInserir.UseVisualStyleBackColor = true;
            this.btnInserir.Click += new System.EventHandler(this.btnInserir_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPessoa);
            this.tabControl1.Controls.Add(this.tabAtleta);
            this.tabControl1.Location = new System.Drawing.Point(12, 41);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(660, 309);
            this.tabControl1.TabIndex = 14;
            // 
            // tabPessoa
            // 
            this.tabPessoa.Controls.Add(this.lblDtNasc);
            this.tabPessoa.Controls.Add(this.dtNasc);
            this.tabPessoa.Controls.Add(this.cbAtleta);
            this.tabPessoa.Controls.Add(this.label11);
            this.tabPessoa.Controls.Add(this.label10);
            this.tabPessoa.Controls.Add(this.cbTipoPessoa);
            this.tabPessoa.Controls.Add(this.tfInscEst);
            this.tabPessoa.Controls.Add(this.lblInscEst);
            this.tabPessoa.Controls.Add(this.tfCNPJ);
            this.tabPessoa.Controls.Add(this.lblCNPJ);
            this.tabPessoa.Controls.Add(this.tfNomeMunicipio);
            this.tabPessoa.Controls.Add(this.btnBuscarMunicipio);
            this.tabPessoa.Controls.Add(this.label7);
            this.tabPessoa.Controls.Add(this.tfIdMunicipio);
            this.tabPessoa.Controls.Add(this.label6);
            this.tabPessoa.Controls.Add(this.tfBairro);
            this.tabPessoa.Controls.Add(this.label5);
            this.tabPessoa.Controls.Add(this.tfNumero);
            this.tabPessoa.Controls.Add(this.label4);
            this.tabPessoa.Controls.Add(this.tfLogradouro);
            this.tabPessoa.Controls.Add(this.tfCEP);
            this.tabPessoa.Controls.Add(this.label3);
            this.tabPessoa.Controls.Add(this.tfRG);
            this.tabPessoa.Controls.Add(this.lblRG);
            this.tabPessoa.Controls.Add(this.tfCPF);
            this.tabPessoa.Controls.Add(this.lblCPF);
            this.tabPessoa.Controls.Add(this.lblDescricao);
            this.tabPessoa.Controls.Add(this.tfNome);
            this.tabPessoa.Controls.Add(this.tfID);
            this.tabPessoa.Controls.Add(this.lblId);
            this.tabPessoa.Location = new System.Drawing.Point(4, 22);
            this.tabPessoa.Name = "tabPessoa";
            this.tabPessoa.Padding = new System.Windows.Forms.Padding(3);
            this.tabPessoa.Size = new System.Drawing.Size(652, 283);
            this.tabPessoa.TabIndex = 0;
            this.tabPessoa.Text = "Pessoa";
            this.tabPessoa.UseVisualStyleBackColor = true;
            // 
            // lblDtNasc
            // 
            this.lblDtNasc.AutoSize = true;
            this.lblDtNasc.Location = new System.Drawing.Point(242, 61);
            this.lblDtNasc.Name = "lblDtNasc";
            this.lblDtNasc.Size = new System.Drawing.Size(79, 13);
            this.lblDtNasc.TabIndex = 49;
            this.lblDtNasc.Text = "Data de Nasc.:";
            this.lblDtNasc.Visible = false;
            // 
            // dtNasc
            // 
            this.dtNasc.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtNasc.Location = new System.Drawing.Point(325, 59);
            this.dtNasc.Name = "dtNasc";
            this.dtNasc.Size = new System.Drawing.Size(107, 20);
            this.dtNasc.TabIndex = 5;
            // 
            // cbAtleta
            // 
            this.cbAtleta.Enabled = false;
            this.cbAtleta.FormattingEnabled = true;
            this.cbAtleta.Items.AddRange(new object[] {
            "Não",
            "Sim"});
            this.cbAtleta.Location = new System.Drawing.Point(76, 244);
            this.cbAtleta.Name = "cbAtleta";
            this.cbAtleta.Size = new System.Drawing.Size(73, 21);
            this.cbAtleta.TabIndex = 16;
            this.cbAtleta.SelectedIndexChanged += new System.EventHandler(this.CbAtleta_SelectedIndexChanged);
            this.cbAtleta.SelectedValueChanged += new System.EventHandler(this.cbAtleta_SelectedValueChanged_1);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(6, 247);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(37, 13);
            this.label11.TabIndex = 47;
            this.label11.Text = "Atleta:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 61);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(69, 13);
            this.label10.TabIndex = 46;
            this.label10.Text = "Tipo Pessoa:";
            // 
            // cbTipoPessoa
            // 
            this.cbTipoPessoa.Enabled = false;
            this.cbTipoPessoa.FormattingEnabled = true;
            this.cbTipoPessoa.Items.AddRange(new object[] {
            "Física",
            "Jurídica"});
            this.cbTipoPessoa.Location = new System.Drawing.Point(76, 58);
            this.cbTipoPessoa.Name = "cbTipoPessoa";
            this.cbTipoPessoa.Size = new System.Drawing.Size(135, 21);
            this.cbTipoPessoa.TabIndex = 4;
            this.cbTipoPessoa.SelectedValueChanged += new System.EventHandler(this.ComboBox1_SelectedValueChanged);
            // 
            // tfInscEst
            // 
            this.tfInscEst.Location = new System.Drawing.Point(325, 111);
            this.tfInscEst.Name = "tfInscEst";
            this.tfInscEst.ReadOnly = true;
            this.tfInscEst.Size = new System.Drawing.Size(160, 20);
            this.tfInscEst.TabIndex = 8;
            this.tfInscEst.Visible = false;
            // 
            // lblInscEst
            // 
            this.lblInscEst.AutoSize = true;
            this.lblInscEst.Location = new System.Drawing.Point(242, 114);
            this.lblInscEst.Name = "lblInscEst";
            this.lblInscEst.Size = new System.Drawing.Size(77, 13);
            this.lblInscEst.TabIndex = 43;
            this.lblInscEst.Text = "Insc. Estadual:";
            this.lblInscEst.Visible = false;
            // 
            // tfCNPJ
            // 
            this.tfCNPJ.Location = new System.Drawing.Point(76, 111);
            this.tfCNPJ.Mask = "00,000,000/0000-00";
            this.tfCNPJ.Name = "tfCNPJ";
            this.tfCNPJ.ReadOnly = true;
            this.tfCNPJ.Size = new System.Drawing.Size(145, 20);
            this.tfCNPJ.TabIndex = 7;
            this.tfCNPJ.Visible = false;
            // 
            // lblCNPJ
            // 
            this.lblCNPJ.AutoSize = true;
            this.lblCNPJ.Location = new System.Drawing.Point(6, 114);
            this.lblCNPJ.Name = "lblCNPJ";
            this.lblCNPJ.Size = new System.Drawing.Size(37, 13);
            this.lblCNPJ.TabIndex = 41;
            this.lblCNPJ.Text = "CNPJ:";
            this.lblCNPJ.Visible = false;
            // 
            // tfNomeMunicipio
            // 
            this.tfNomeMunicipio.Location = new System.Drawing.Point(227, 217);
            this.tfNomeMunicipio.Name = "tfNomeMunicipio";
            this.tfNomeMunicipio.ReadOnly = true;
            this.tfNomeMunicipio.Size = new System.Drawing.Size(419, 20);
            this.tfNomeMunicipio.TabIndex = 15;
            // 
            // btnBuscarMunicipio
            // 
            this.btnBuscarMunicipio.Location = new System.Drawing.Point(155, 215);
            this.btnBuscarMunicipio.Name = "btnBuscarMunicipio";
            this.btnBuscarMunicipio.Size = new System.Drawing.Size(66, 23);
            this.btnBuscarMunicipio.TabIndex = 14;
            this.btnBuscarMunicipio.Text = "Buscar";
            this.btnBuscarMunicipio.UseVisualStyleBackColor = true;
            this.btnBuscarMunicipio.Click += new System.EventHandler(this.BtnBuscarMunicipio_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 220);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(57, 13);
            this.label7.TabIndex = 38;
            this.label7.Text = "Município:";
            // 
            // tfIdMunicipio
            // 
            this.tfIdMunicipio.Location = new System.Drawing.Point(76, 217);
            this.tfIdMunicipio.Name = "tfIdMunicipio";
            this.tfIdMunicipio.ReadOnly = true;
            this.tfIdMunicipio.Size = new System.Drawing.Size(73, 20);
            this.tfIdMunicipio.TabIndex = 13;
            this.tfIdMunicipio.Leave += new System.EventHandler(this.TfIdMunicipio_Leave);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(231, 192);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(37, 13);
            this.label6.TabIndex = 36;
            this.label6.Text = "Bairro:";
            // 
            // tfBairro
            // 
            this.tfBairro.Location = new System.Drawing.Point(274, 189);
            this.tfBairro.Name = "tfBairro";
            this.tfBairro.ReadOnly = true;
            this.tfBairro.Size = new System.Drawing.Size(145, 20);
            this.tfBairro.TabIndex = 12;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 192);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(47, 13);
            this.label5.TabIndex = 34;
            this.label5.Text = "Número:";
            // 
            // tfNumero
            // 
            this.tfNumero.Location = new System.Drawing.Point(76, 189);
            this.tfNumero.Name = "tfNumero";
            this.tfNumero.ReadOnly = true;
            this.tfNumero.Size = new System.Drawing.Size(145, 20);
            this.tfNumero.TabIndex = 11;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 166);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(64, 13);
            this.label4.TabIndex = 32;
            this.label4.Text = "Logradouro:";
            // 
            // tfLogradouro
            // 
            this.tfLogradouro.Location = new System.Drawing.Point(76, 163);
            this.tfLogradouro.Name = "tfLogradouro";
            this.tfLogradouro.ReadOnly = true;
            this.tfLogradouro.Size = new System.Drawing.Size(570, 20);
            this.tfLogradouro.TabIndex = 10;
            // 
            // tfCEP
            // 
            this.tfCEP.Location = new System.Drawing.Point(76, 137);
            this.tfCEP.Mask = "00000-000";
            this.tfCEP.Name = "tfCEP";
            this.tfCEP.ReadOnly = true;
            this.tfCEP.Size = new System.Drawing.Size(145, 20);
            this.tfCEP.TabIndex = 9;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 140);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(31, 13);
            this.label3.TabIndex = 29;
            this.label3.Text = "CEP:";
            // 
            // tfRG
            // 
            this.tfRG.Location = new System.Drawing.Point(325, 85);
            this.tfRG.Name = "tfRG";
            this.tfRG.ReadOnly = true;
            this.tfRG.Size = new System.Drawing.Size(130, 20);
            this.tfRG.TabIndex = 6;
            this.tfRG.Visible = false;
            // 
            // lblRG
            // 
            this.lblRG.AutoSize = true;
            this.lblRG.Location = new System.Drawing.Point(242, 88);
            this.lblRG.Name = "lblRG";
            this.lblRG.Size = new System.Drawing.Size(26, 13);
            this.lblRG.TabIndex = 27;
            this.lblRG.Text = "RG:";
            this.lblRG.Visible = false;
            // 
            // tfCPF
            // 
            this.tfCPF.Location = new System.Drawing.Point(76, 85);
            this.tfCPF.Mask = "000,000,000-00";
            this.tfCPF.Name = "tfCPF";
            this.tfCPF.ReadOnly = true;
            this.tfCPF.Size = new System.Drawing.Size(145, 20);
            this.tfCPF.TabIndex = 5;
            this.tfCPF.Visible = false;
            // 
            // lblCPF
            // 
            this.lblCPF.AutoSize = true;
            this.lblCPF.Location = new System.Drawing.Point(6, 88);
            this.lblCPF.Name = "lblCPF";
            this.lblCPF.Size = new System.Drawing.Size(30, 13);
            this.lblCPF.TabIndex = 25;
            this.lblCPF.Text = "CPF:";
            this.lblCPF.Visible = false;
            // 
            // lblDescricao
            // 
            this.lblDescricao.AutoSize = true;
            this.lblDescricao.Location = new System.Drawing.Point(6, 35);
            this.lblDescricao.Name = "lblDescricao";
            this.lblDescricao.Size = new System.Drawing.Size(38, 13);
            this.lblDescricao.TabIndex = 23;
            this.lblDescricao.Text = "Nome:";
            // 
            // tfNome
            // 
            this.tfNome.Location = new System.Drawing.Point(76, 32);
            this.tfNome.Name = "tfNome";
            this.tfNome.ReadOnly = true;
            this.tfNome.Size = new System.Drawing.Size(570, 20);
            this.tfNome.TabIndex = 3;
            // 
            // tfID
            // 
            this.tfID.Location = new System.Drawing.Point(76, 6);
            this.tfID.Name = "tfID";
            this.tfID.ReadOnly = true;
            this.tfID.Size = new System.Drawing.Size(60, 20);
            this.tfID.TabIndex = 18;
            // 
            // lblId
            // 
            this.lblId.AutoSize = true;
            this.lblId.Location = new System.Drawing.Point(6, 9);
            this.lblId.Name = "lblId";
            this.lblId.Size = new System.Drawing.Size(19, 13);
            this.lblId.TabIndex = 17;
            this.lblId.Text = "Id:";
            // 
            // tabAtleta
            // 
            this.tabAtleta.Controls.Add(this.tfNomeNacionalidade);
            this.tabAtleta.Controls.Add(this.btnBuscarNacionalidade);
            this.tabAtleta.Controls.Add(this.tfIdNacionalidade);
            this.tabAtleta.Controls.Add(this.cbAtivo);
            this.tabAtleta.Controls.Add(this.label17);
            this.tabAtleta.Controls.Add(this.cbIsentoMens);
            this.tabAtleta.Controls.Add(this.label16);
            this.tabAtleta.Controls.Add(this.label15);
            this.tabAtleta.Controls.Add(this.numCalcado);
            this.tabAtleta.Controls.Add(this.cbTamCalca);
            this.tabAtleta.Controls.Add(this.label14);
            this.tabAtleta.Controls.Add(this.cbTamJersey);
            this.tabAtleta.Controls.Add(this.label13);
            this.tabAtleta.Controls.Add(this.cbTamShoulder);
            this.tabAtleta.Controls.Add(this.label12);
            this.tabAtleta.Controls.Add(this.cbTamHelmet);
            this.tabAtleta.Controls.Add(this.label9);
            this.tabAtleta.Controls.Add(this.label8);
            this.tabAtleta.Controls.Add(this.tfAltura);
            this.tabAtleta.Controls.Add(this.label2);
            this.tabAtleta.Controls.Add(this.tfPeso);
            this.tabAtleta.Controls.Add(this.label18);
            this.tabAtleta.Location = new System.Drawing.Point(4, 22);
            this.tabAtleta.Name = "tabAtleta";
            this.tabAtleta.Padding = new System.Windows.Forms.Padding(3);
            this.tabAtleta.Size = new System.Drawing.Size(652, 283);
            this.tabAtleta.TabIndex = 1;
            this.tabAtleta.Text = "Atleta";
            this.tabAtleta.UseVisualStyleBackColor = true;
            // 
            // tfNomeNacionalidade
            // 
            this.tfNomeNacionalidade.Location = new System.Drawing.Point(220, 139);
            this.tfNomeNacionalidade.Name = "tfNomeNacionalidade";
            this.tfNomeNacionalidade.ReadOnly = true;
            this.tfNomeNacionalidade.Size = new System.Drawing.Size(426, 20);
            this.tfNomeNacionalidade.TabIndex = 41;
            // 
            // btnBuscarNacionalidade
            // 
            this.btnBuscarNacionalidade.Location = new System.Drawing.Point(148, 137);
            this.btnBuscarNacionalidade.Name = "btnBuscarNacionalidade";
            this.btnBuscarNacionalidade.Size = new System.Drawing.Size(66, 23);
            this.btnBuscarNacionalidade.TabIndex = 40;
            this.btnBuscarNacionalidade.Text = "Buscar";
            this.btnBuscarNacionalidade.UseVisualStyleBackColor = true;
            this.btnBuscarNacionalidade.Click += new System.EventHandler(this.BtnBuscarNacionalidade_Click);
            // 
            // tfIdNacionalidade
            // 
            this.tfIdNacionalidade.Location = new System.Drawing.Point(82, 138);
            this.tfIdNacionalidade.Name = "tfIdNacionalidade";
            this.tfIdNacionalidade.ReadOnly = true;
            this.tfIdNacionalidade.Size = new System.Drawing.Size(60, 20);
            this.tfIdNacionalidade.TabIndex = 39;
            this.tfIdNacionalidade.Leave += new System.EventHandler(this.TfIdNacionalidade_Leave);
            // 
            // cbAtivo
            // 
            this.cbAtivo.Enabled = false;
            this.cbAtivo.FormattingEnabled = true;
            this.cbAtivo.Items.AddRange(new object[] {
            "Não",
            "Sim"});
            this.cbAtivo.Location = new System.Drawing.Point(191, 111);
            this.cbAtivo.Name = "cbAtivo";
            this.cbAtivo.Size = new System.Drawing.Size(60, 21);
            this.cbAtivo.TabIndex = 38;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(151, 114);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(34, 13);
            this.label17.TabIndex = 37;
            this.label17.Text = "Ativo:";
            // 
            // cbIsentoMens
            // 
            this.cbIsentoMens.Enabled = false;
            this.cbIsentoMens.FormattingEnabled = true;
            this.cbIsentoMens.Items.AddRange(new object[] {
            "Não",
            "Sim"});
            this.cbIsentoMens.Location = new System.Drawing.Point(82, 111);
            this.cbIsentoMens.Name = "cbIsentoMens";
            this.cbIsentoMens.Size = new System.Drawing.Size(60, 21);
            this.cbIsentoMens.TabIndex = 36;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(6, 114);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(71, 13);
            this.label16.TabIndex = 35;
            this.label16.Text = "Isento Mens.:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(6, 87);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(67, 13);
            this.label15.TabIndex = 34;
            this.label15.Text = "N° Calçado: ";
            // 
            // numCalcado
            // 
            this.numCalcado.Location = new System.Drawing.Point(82, 85);
            this.numCalcado.Name = "numCalcado";
            this.numCalcado.Size = new System.Drawing.Size(60, 20);
            this.numCalcado.TabIndex = 33;
            this.numCalcado.Value = new decimal(new int[] {
            42,
            0,
            0,
            0});
            // 
            // cbTamCalca
            // 
            this.cbTamCalca.FormattingEnabled = true;
            this.cbTamCalca.Items.AddRange(new object[] {
            "S",
            "M",
            "L",
            "XL",
            "XXL",
            "XXXL"});
            this.cbTamCalca.Location = new System.Drawing.Point(508, 58);
            this.cbTamCalca.Name = "cbTamCalca";
            this.cbTamCalca.Size = new System.Drawing.Size(60, 21);
            this.cbTamCalca.TabIndex = 32;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(438, 61);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(64, 13);
            this.label14.TabIndex = 31;
            this.label14.Text = "Tam. Calça:";
            // 
            // cbTamJersey
            // 
            this.cbTamJersey.FormattingEnabled = true;
            this.cbTamJersey.Items.AddRange(new object[] {
            "S",
            "M",
            "L",
            "XL",
            "XXL",
            "XXXL"});
            this.cbTamJersey.Location = new System.Drawing.Point(372, 58);
            this.cbTamJersey.Name = "cbTamJersey";
            this.cbTamJersey.Size = new System.Drawing.Size(60, 21);
            this.cbTamJersey.TabIndex = 30;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(299, 61);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(67, 13);
            this.label13.TabIndex = 29;
            this.label13.Text = "Tam. Jersey:";
            // 
            // cbTamShoulder
            // 
            this.cbTamShoulder.FormattingEnabled = true;
            this.cbTamShoulder.Items.AddRange(new object[] {
            "S",
            "M",
            "L",
            "XL",
            "XXL",
            "XXXL"});
            this.cbTamShoulder.Location = new System.Drawing.Point(233, 58);
            this.cbTamShoulder.Name = "cbTamShoulder";
            this.cbTamShoulder.Size = new System.Drawing.Size(60, 21);
            this.cbTamShoulder.TabIndex = 28;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(148, 61);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(79, 13);
            this.label12.TabIndex = 27;
            this.label12.Text = "Tam. Shoulder:";
            // 
            // cbTamHelmet
            // 
            this.cbTamHelmet.FormattingEnabled = true;
            this.cbTamHelmet.Items.AddRange(new object[] {
            "S",
            "M",
            "L",
            "XL",
            "XXL",
            "XXXL"});
            this.cbTamHelmet.Location = new System.Drawing.Point(82, 58);
            this.cbTamHelmet.Name = "cbTamHelmet";
            this.cbTamHelmet.Size = new System.Drawing.Size(60, 21);
            this.cbTamHelmet.TabIndex = 26;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 61);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(70, 13);
            this.label9.TabIndex = 25;
            this.label9.Text = "Tam. Helmet:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(148, 36);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(37, 13);
            this.label8.TabIndex = 24;
            this.label8.Text = "Altura:";
            // 
            // tfAltura
            // 
            this.tfAltura.Location = new System.Drawing.Point(191, 33);
            this.tfAltura.Name = "tfAltura";
            this.tfAltura.Size = new System.Drawing.Size(60, 20);
            this.tfAltura.TabIndex = 23;
            this.tfAltura.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TfAltura_KeyPress);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 36);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(34, 13);
            this.label2.TabIndex = 22;
            this.label2.Text = "Peso:";
            // 
            // tfPeso
            // 
            this.tfPeso.Location = new System.Drawing.Point(82, 32);
            this.tfPeso.Name = "tfPeso";
            this.tfPeso.Size = new System.Drawing.Size(60, 20);
            this.tfPeso.TabIndex = 21;
            this.tfPeso.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TfPeso_KeyPress);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(6, 141);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(78, 13);
            this.label18.TabIndex = 42;
            this.label18.Text = "Nacionalidade:";
            // 
            // tfFiltro
            // 
            this.tfFiltro.Location = new System.Drawing.Point(131, 385);
            this.tfFiltro.Name = "tfFiltro";
            this.tfFiltro.Size = new System.Drawing.Size(541, 20);
            this.tfFiltro.TabIndex = 39;
            this.tfFiltro.TextChanged += new System.EventHandler(this.TfFiltro_TextChanged);
            // 
            // cbFiltro
            // 
            this.cbFiltro.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cbFiltro.FormattingEnabled = true;
            this.cbFiltro.Location = new System.Drawing.Point(12, 385);
            this.cbFiltro.Name = "cbFiltro";
            this.cbFiltro.Size = new System.Drawing.Size(112, 21);
            this.cbFiltro.TabIndex = 38;
            // 
            // btnCancelar
            // 
            this.btnCancelar.Location = new System.Drawing.Point(345, 356);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(75, 23);
            this.btnCancelar.TabIndex = 18;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.BtnCancelar_Click_1);
            // 
            // btnSalvar
            // 
            this.btnSalvar.Location = new System.Drawing.Point(264, 356);
            this.btnSalvar.Name = "btnSalvar";
            this.btnSalvar.Size = new System.Drawing.Size(75, 23);
            this.btnSalvar.TabIndex = 17;
            this.btnSalvar.Text = "Salvar";
            this.btnSalvar.UseVisualStyleBackColor = true;
            this.btnSalvar.Click += new System.EventHandler(this.BtnSalvar_Click_1);
            // 
            // gridItems
            // 
            this.gridItems.AllowUserToAddRows = false;
            this.gridItems.AllowUserToDeleteRows = false;
            this.gridItems.AllowUserToOrderColumns = true;
            this.gridItems.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridItems.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridItems.Location = new System.Drawing.Point(12, 412);
            this.gridItems.Name = "gridItems";
            this.gridItems.ReadOnly = true;
            this.gridItems.Size = new System.Drawing.Size(660, 237);
            this.gridItems.TabIndex = 35;
            // 
            // frmPessoaAtleta
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(684, 661);
            this.Controls.Add(this.tfFiltro);
            this.Controls.Add(this.cbFiltro);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.btnSalvar);
            this.Controls.Add(this.gridItems);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.btnExcluir);
            this.Controls.Add(this.btnAlterar);
            this.Controls.Add(this.btnInserir);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "frmPessoaAtleta";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Cadastro de Pessoa/Atleta";
            this.tabControl1.ResumeLayout(false);
            this.tabPessoa.ResumeLayout(false);
            this.tabPessoa.PerformLayout();
            this.tabAtleta.ResumeLayout(false);
            this.tabAtleta.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numCalcado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridItems)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnExcluir;
        private System.Windows.Forms.Button btnAlterar;
        private System.Windows.Forms.Button btnInserir;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPessoa;
        private System.Windows.Forms.TabPage tabAtleta;
        private System.Windows.Forms.TextBox tfFiltro;
        private System.Windows.Forms.ComboBox cbFiltro;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Button btnSalvar;
        private System.Windows.Forms.DataGridView gridItems;
        private System.Windows.Forms.TextBox tfID;
        private System.Windows.Forms.Label lblId;
        private System.Windows.Forms.Label lblDescricao;
        private System.Windows.Forms.TextBox tfNome;
        private System.Windows.Forms.Label lblCPF;
        private System.Windows.Forms.MaskedTextBox tfRG;
        private System.Windows.Forms.Label lblRG;
        private System.Windows.Forms.MaskedTextBox tfCPF;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tfBairro;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tfNumero;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tfLogradouro;
        private System.Windows.Forms.MaskedTextBox tfCEP;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox tfIdMunicipio;
        private System.Windows.Forms.TextBox tfNomeMunicipio;
        private System.Windows.Forms.Button btnBuscarMunicipio;
        private System.Windows.Forms.MaskedTextBox tfInscEst;
        private System.Windows.Forms.Label lblInscEst;
        private System.Windows.Forms.MaskedTextBox tfCNPJ;
        private System.Windows.Forms.Label lblCNPJ;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox cbTipoPessoa;
        private System.Windows.Forms.ComboBox cbAtleta;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.MaskedTextBox tfPeso;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.MaskedTextBox tfAltura;
        private System.Windows.Forms.ComboBox cbTamHelmet;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox cbTamJersey;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ComboBox cbTamShoulder;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox cbTamCalca;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.NumericUpDown numCalcado;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ComboBox cbIsentoMens;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.ComboBox cbAtivo;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox tfNomeNacionalidade;
        private System.Windows.Forms.Button btnBuscarNacionalidade;
        private System.Windows.Forms.TextBox tfIdNacionalidade;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.DateTimePicker dtNasc;
        private System.Windows.Forms.Label lblDtNasc;
    }
}