﻿using GestaoFABR.Control;
using GestaoFABR.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestaoFABR.View
{
    public partial class frmProduto : Form
    {
        private bool isAtualizaCadastro = false;
        private ControlProduto control = new ControlProduto();


        public void desabilitaTudo()
        {
            tfID.Text = "";
            tfNome.Text = "";
            tfValor.Text = "";
            tfID.ReadOnly = true;
            tfNome.ReadOnly = true;
            tfValor.ReadOnly = true;
            btnAlterar.Enabled = false;
            btnCancelar.Enabled = false;
            btnExcluir.Enabled = false;
            btnInserir.Enabled = false;
            btnSalvar.Enabled = false;
        }

        public void estadoInicial()
        {
            desabilitaTudo();
            preencheGrid();
            btnInserir.Enabled = true;
            btnAlterar.Enabled = true;
            btnExcluir.Enabled = true;
        }

        public void preencheGrid()
        {
            gridItems.DataSource = control.obter();
        }

        public frmProduto()
        {
            InitializeComponent();
            estadoInicial();
            foreach (DataColumn coluna in (gridItems.DataSource as DataTable).Columns)
            {
                cbFiltro.Items.Add(coluna.ColumnName);
            }
            cbFiltro.SelectedIndex = 0;
        }


        private void BtnSalvar_Click(object sender, EventArgs e)
        {
            if (isAtualizaCadastro)
            {
                bool result = control.atualiza(new Produto(Int32.Parse(tfID.Text), tfNome.Text, float.Parse(tfValor.Text)));
                if (!result)
                {
                    MessageBox.Show("Ocorreu um erro ao alterar o registro.", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    estadoInicial();
                    preencheGrid();
                }
            }
            else
            {
                bool result = control.insere(new Produto(tfNome.Text, float.Parse(tfValor.Text)));
                if (!result)
                {
                    MessageBox.Show("Ocorreu um erro ao inserir o registro.", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    estadoInicial();
                    preencheGrid();
                }
            }

        }

        private void BtnCancelar_Click(object sender, EventArgs e)
        {
            estadoInicial();
        }

        private void TfFiltro_TextChanged(object sender, EventArgs e)
        {
            Utils.filtraGrid(gridItems, cbFiltro, tfFiltro);
        }

        private void BtnInserir_Click(object sender, EventArgs e)
        {
            isAtualizaCadastro = false;
            desabilitaTudo();
            btnCancelar.Enabled = true;
            btnSalvar.Enabled = true;
            tfNome.ReadOnly = false;
            tfValor.ReadOnly = false;
            tfNome.Focus();
        }

        private void BtnAlterar_Click(object sender, EventArgs e)
        {
            if (gridItems.SelectedCells.Count > 0)
            {
                isAtualizaCadastro = true;
                desabilitaTudo();
                btnCancelar.Enabled = true;
                btnSalvar.Enabled = true;
                tfNome.ReadOnly = false;
                tfValor.ReadOnly = false;
                tfID.Text = gridItems.Rows[gridItems.SelectedCells[0].RowIndex].Cells["id"].Value.ToString();
                tfNome.Text = gridItems.Rows[gridItems.SelectedCells[0].RowIndex].Cells["descricao"].Value.ToString();
                tfValor.Text = gridItems.Rows[gridItems.SelectedCells[0].RowIndex].Cells["valor"].Value.ToString();
                tfNome.Focus();
            }
            else
            {
                MessageBox.Show("Por favor, selecione um registro.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void BtnExcluir_Click(object sender, EventArgs e)
        {
            if (gridItems.SelectedCells.Count > 0)
            {
                if (MessageBox.Show("Tem certeza que deseja excluir o registro?", "Aviso", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    bool result = control.remove(Int32.Parse(gridItems.Rows[gridItems.SelectedCells[0].RowIndex].Cells["Id"].Value.ToString()));
                    if (!result)
                    {
                        MessageBox.Show("Ocorreu um erro ao excluir o registro.", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                    {
                        estadoInicial();
                        preencheGrid();
                    }

                }
            }
            else
            {
                MessageBox.Show("Por favor, selecione um registro.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void tfValor_KeyPress(object sender, KeyPressEventArgs e)
        {
            Utils.formataNumericEventTextBox(sender, e);
        }
    }
}
