﻿using GestaoFABR.Control;
using GestaoFABR.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestaoFABR.View
{
    public partial class frmContaBancaria : Form
    {
        private bool isAtualizaCadastro = false;
        private ControlContaBancaria control = new ControlContaBancaria();
        public void desabilitaTudo()
        {
            tfID.Text = "";
            tfDescricao.Text = "";
            cbTipoConta.SelectedIndex = -1;
            tfConta.Text = "";
            tfAgencia.Text = "";
            tfBeneficiario.Text = "";
            tfFiltro.Text = "";
            tfID.ReadOnly = true;
            tfDescricao.ReadOnly = true;
            cbTipoConta.Enabled = false;
            tfConta.ReadOnly = true;
            tfAgencia.ReadOnly = true;
            tfBeneficiario.ReadOnly = true;
            btnAlterar.Enabled = false;
            btnCancelar.Enabled = false;
            btnExcluir.Enabled = false;
            btnInserir.Enabled = false;
            btnSalvar.Enabled = false;
        }
        public void estadoInicial()
        {
            desabilitaTudo();
            btnInserir.Enabled = true;
            btnAlterar.Enabled = true;
            btnExcluir.Enabled = true;
        }
        public void preencheGrid()
        {
            gridItems.DataSource = control.obter();
        }
        
        public frmContaBancaria()
        {
            InitializeComponent();
            estadoInicial();
            preencheGrid();
            cbTipoConta.Items.AddRange(new String[] { "Conta Corrente", "Conta Poupança", "Caixa Registradora" });
            foreach (DataColumn coluna in (gridItems.DataSource as DataTable).Columns)
            {
                cbFiltro.Items.Add(coluna.ColumnName);
            }
            cbFiltro.SelectedIndex = 0;
        }

        private void btnInserir_Click(object sender, EventArgs e)
        {
            isAtualizaCadastro = false;
            desabilitaTudo();
            btnCancelar.Enabled = true;
            btnSalvar.Enabled = true;
            tfDescricao.ReadOnly = false;
            cbTipoConta.Enabled = true;
            tfConta.ReadOnly = false;
            tfAgencia.ReadOnly = false;
            tfBeneficiario.ReadOnly = false;
            cbTipoConta.SelectedIndex = 0;
            tfDescricao.Focus();
        }

        private void btnAlterar_Click(object sender, EventArgs e)
        {
            if (gridItems.SelectedCells.Count > 0)
            {
                isAtualizaCadastro = true;
                desabilitaTudo();
                btnCancelar.Enabled = true;
                btnSalvar.Enabled = true;
                tfDescricao.ReadOnly = false;
                cbTipoConta.Enabled = true;
                tfConta.ReadOnly = false;
                tfAgencia.ReadOnly = false;
                tfBeneficiario.ReadOnly = false;
                tfDescricao.Focus();
                tfID.Text = gridItems.Rows[gridItems.SelectedCells[0].RowIndex].Cells["id"].Value.ToString();
                tfDescricao.Text = gridItems.Rows[gridItems.SelectedCells[0].RowIndex].Cells["descricao"].Value.ToString();
                cbTipoConta.SelectedIndex = control.retornaTipoInt(char.Parse(gridItems.Rows[gridItems.SelectedCells[0].RowIndex].Cells["tipo"].Value.ToString()));
                tfConta.Text = gridItems.Rows[gridItems.SelectedCells[0].RowIndex].Cells["conta"].Value.ToString();
                tfAgencia.Text = gridItems.Rows[gridItems.SelectedCells[0].RowIndex].Cells["agencia"].Value.ToString();
                tfBeneficiario.Text = gridItems.Rows[gridItems.SelectedCells[0].RowIndex].Cells["beneficiario"].Value.ToString();                
                tfDescricao.Focus();
            }
            else
            {
                MessageBox.Show("Por favor, selecione um registro.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            if(isAtualizaCadastro)
            {
                bool result = control.atualiza(new ContaBancaria(Int32.Parse(tfID.Text), tfDescricao.Text, control.retornaTipoChar(cbTipoConta.SelectedIndex), tfConta.Text, tfAgencia.Text, tfBeneficiario.Text));
                if (!result)
                {
                    MessageBox.Show("Ocorreu um erro ao alterar o registro.", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    estadoInicial();
                    preencheGrid();
                }
            } else
            {
                bool result = control.insere(new ContaBancaria(tfDescricao.Text, control.retornaTipoChar(cbTipoConta.SelectedIndex), tfConta.Text, tfAgencia.Text, tfBeneficiario.Text));
                if (!result)
                {
                    MessageBox.Show("Ocorreu um erro ao inserir o registro.", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    estadoInicial();
                    preencheGrid();
                }
            }
        }

        private void btnExcluir_Click(object sender, EventArgs e)
        {
            if (gridItems.SelectedCells.Count > 0)
            {
                if (MessageBox.Show("Tem certeza que deseja excluir o registro?", "Aviso", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    bool result = control.remove(Int32.Parse(gridItems.Rows[gridItems.SelectedCells[0].RowIndex].Cells["Id"].Value.ToString()));
                    if (!result)
                    {
                        MessageBox.Show("Ocorreu um erro ao excluir o registro.", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                    {
                        estadoInicial();
                        preencheGrid();
                    }

                }
            }
            else
            {
                MessageBox.Show("Por favor, selecione um registro.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            estadoInicial();
        }

        private void tfFiltro_TextChanged(object sender, EventArgs e)
        {
            Utils.filtraGrid(gridItems, cbFiltro, tfFiltro);
        }
    }
}
