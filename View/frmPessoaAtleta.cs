﻿using GestaoFABR.Control;
using GestaoFABR.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestaoFABR.View
{
    public partial class frmPessoaAtleta : Form
    {
        private bool isAtualizaCadastro = false;
        private ControlPessoa controlPessoa = new ControlPessoa();
        private ControlMunicipio controlMunicipio = new ControlMunicipio();
        private ControlNacionalidade controlNacionalidade = new ControlNacionalidade();

        public void habilitaTudoPessoa()
        {
            desabilitaTudoPessoa();
            btnCancelar.Enabled = true;
            btnSalvar.Enabled = true;
            tfNome.ReadOnly = false;
            cbTipoPessoa.Enabled = true;
            tfCPF.ReadOnly = false;
            tfRG.ReadOnly = false;
            tfInscEst.ReadOnly = true;
            dtNasc.Enabled = true;
            tfCNPJ.ReadOnly = false;
            tfCEP.ReadOnly = false;
            tfLogradouro.ReadOnly = false;
            tfNumero.ReadOnly = false;
            tfBairro.ReadOnly = false;
            tfIdMunicipio.ReadOnly = false;
            btnBuscarMunicipio.Enabled = true;
            cbAtleta.Enabled = true;
            cbAtleta.SelectedIndex = 0;
            cbTipoPessoa.SelectedIndex = 0;
            tfNome.Focus();
        }

        public void habilitaTudoAtleta()
        {
            btnBuscarNacionalidade.Enabled = true;
            tfPeso.ReadOnly = false;
            tfAltura.ReadOnly = false;
            cbTamHelmet.Enabled = true;
            cbTamShoulder.Enabled = true;
            cbTamJersey.Enabled = true;
            cbTamCalca.Enabled = true;
            numCalcado.Enabled = true;
            cbIsentoMens.Enabled = true;
            cbAtivo.Enabled = true;
            tfIdNacionalidade.ReadOnly = false;
        }

        public void desabilitaTudoPessoa()
        {
            //Limpa campos
            tfID.Text = "";
            tfNome.Text = "";
            cbTipoPessoa.SelectedIndex = -1;
            tfCPF.Text = "";
            tfRG.Text = "";
            dtNasc.Value = DateTime.Today;
            tfCNPJ.Text = "";
            tfInscEst.Text = "";
            tfCEP.Text = "";
            tfLogradouro.Text = "";
            tfNumero.Text = "";
            tfBairro.Text = "";
            tfIdMunicipio.Text = "";
            tfNomeMunicipio.Text = "";
            cbAtleta.SelectedIndex = -1;
            tfFiltro.Text = "";
            //Desativa campos
            tfID.ReadOnly = true;
            tfNome.ReadOnly = true;
            cbTipoPessoa.Enabled = false;
            tfCPF.ReadOnly = true;
            tfRG.ReadOnly = true;
            tfRG.ReadOnly = false;
            dtNasc.Enabled = false;
            tfCNPJ.ReadOnly = true;
            tfInscEst.ReadOnly = true;
            tfCEP.ReadOnly = true;
            tfLogradouro.ReadOnly = true;
            tfNumero.ReadOnly = true;
            tfBairro.ReadOnly = true;
            tfIdMunicipio.ReadOnly = true;
            btnBuscarMunicipio.Enabled = false;
            tfNomeMunicipio.ReadOnly = true;
            cbAtleta.Enabled = false;
            btnAlterar.Enabled = false;
            btnCancelar.Enabled = false;
            btnExcluir.Enabled = false;
            btnInserir.Enabled = false;
            btnSalvar.Enabled = false;
        }

        public void desabilitaTudoAtleta()
        {
            tfPeso.Text = "";
            tfAltura.Text = "";
            cbTamHelmet.SelectedIndex = -1;
            cbTamShoulder.SelectedIndex = -1;
            cbTamJersey.SelectedIndex = -1;
            cbTamCalca.SelectedIndex = -1;
            numCalcado.Value = 42;
            cbIsentoMens.SelectedIndex = -1;
            cbAtivo.SelectedIndex = -1;
            tfIdNacionalidade.Text = "";
            tfNomeNacionalidade.Text = "";
            btnBuscarNacionalidade.Enabled = false;
            tfPeso.ReadOnly = true;
            tfAltura.ReadOnly = true;
            cbTamHelmet.Enabled = false;
            cbTamShoulder.Enabled = false;
            cbTamJersey.Enabled = false;
            cbTamCalca.Enabled = false;
            numCalcado.Enabled = false;
            cbIsentoMens.Enabled = false;
            cbAtivo.Enabled = false;
            tfIdNacionalidade.ReadOnly = true;
            
        }

        public void estadoInicial()
        {
            desabilitaTudoPessoa();
            desabilitaTudoAtleta();
            preencheGrid();
            btnInserir.Enabled = true;
            btnAlterar.Enabled = true;
            btnExcluir.Enabled = true;
        }
        public void preencheGrid()
        {
            gridItems.DataSource = controlPessoa.obter();
        }

        public bool validaCampos()
        {
            if (string.IsNullOrWhiteSpace(tfIdMunicipio.Text))
            {
                return false;
            } else
            {
                return true;
            }
        }

        public frmPessoaAtleta()
        {
            InitializeComponent();
            estadoInicial();
            foreach (DataColumn coluna in (gridItems.DataSource as DataTable).Columns)
            {
                cbFiltro.Items.Add(coluna.ColumnName);
            }
            cbFiltro.SelectedIndex = 0;


        }


        private void ComboBox1_SelectedValueChanged(object sender, EventArgs e)
        {
            if (cbTipoPessoa.SelectedIndex == -1)
            {
                tfCPF.Hide();
                tfRG.Hide();
                lblCPF.Hide();
                lblRG.Hide();
                lblDtNasc.Hide();

                tfCNPJ.Hide();
                tfInscEst.Hide();
                lblCNPJ.Hide();
                lblInscEst.Hide();
            } else
            {
                if (cbTipoPessoa.SelectedItem.ToString() == "Física")
                {
                    tfCNPJ.Hide();
                    tfInscEst.Hide();
                    lblCNPJ.Hide();
                    lblInscEst.Hide();

                    tfCPF.Show();
                    tfRG.Show();
                    lblCPF.Show();
                    lblRG.Show();
                    lblDtNasc.Show();
                }
                else if (cbTipoPessoa.SelectedItem.ToString() == "Jurídica")
                {
                    tfCPF.Hide();
                    tfRG.Hide();
                    lblCPF.Hide();
                    lblRG.Hide();
                    lblDtNasc.Hide();

                    tfCNPJ.Show();
                    tfInscEst.Show();
                    lblCNPJ.Show();
                    lblInscEst.Show();
                }
            }
        }

        private void btnInserir_Click(object sender, EventArgs e)
        {
            isAtualizaCadastro = false;
            habilitaTudoPessoa();
        }

        private void BtnCancelar_Click_1(object sender, EventArgs e)
        {
            estadoInicial();
        }

        private void BtnAlterar_Click(object sender, EventArgs e)
        {
            isAtualizaCadastro = true;
            if (gridItems.SelectedCells.Count > 0)
            {
                isAtualizaCadastro = true;
                btnCancelar.Enabled = true;
                btnSalvar.Enabled = true;
                habilitaTudoPessoa();                
                tfID.Text = gridItems.Rows[gridItems.SelectedCells[0].RowIndex].Cells["id"].Value.ToString();
                tfNome.Text = gridItems.Rows[gridItems.SelectedCells[0].RowIndex].Cells["nome"].Value.ToString();
                cbTipoPessoa.SelectedIndex = controlPessoa.retornaTipoPessoaInt(char.Parse(gridItems.Rows[gridItems.SelectedCells[0].RowIndex].Cells["tipo_pessoa"].Value.ToString()));
                dtNasc.Value = Utils.stringToDtPicker(gridItems.Rows[gridItems.SelectedCells[0].RowIndex].Cells["data_nascimento"].Value.ToString());

                if (gridItems.Rows[gridItems.SelectedCells[0].RowIndex].Cells["tipo_pessoa"].Value.ToString() == "F")
                {
                    tfCPF.Text = gridItems.Rows[gridItems.SelectedCells[0].RowIndex].Cells["cnpj_cpf"].Value.ToString();
                    tfRG.Text = gridItems.Rows[gridItems.SelectedCells[0].RowIndex].Cells["rg_insc_est"].Value.ToString();
                } else
                {
                    tfCNPJ.Text = gridItems.Rows[gridItems.SelectedCells[0].RowIndex].Cells["cnpj_cpf"].Value.ToString();
                    tfInscEst.Text = gridItems.Rows[gridItems.SelectedCells[0].RowIndex].Cells["rg_insc_est"].Value.ToString();
                }
                tfCEP.Text = gridItems.Rows[gridItems.SelectedCells[0].RowIndex].Cells["cep"].Value.ToString();
                tfLogradouro.Text = gridItems.Rows[gridItems.SelectedCells[0].RowIndex].Cells["logradouro"].Value.ToString();
                tfNumero.Text = gridItems.Rows[gridItems.SelectedCells[0].RowIndex].Cells["numero"].Value.ToString();
                tfBairro.Text = gridItems.Rows[gridItems.SelectedCells[0].RowIndex].Cells["bairro"].Value.ToString();
                tfIdMunicipio.Text = gridItems.Rows[gridItems.SelectedCells[0].RowIndex].Cells["municipio_id"].Value.ToString();
                cbAtleta.SelectedItem = gridItems.Rows[gridItems.SelectedCells[0].RowIndex].Cells["atleta"].Value.ToString();
                if (!String.IsNullOrWhiteSpace(tfIdMunicipio.Text))
                {
                    tfNomeMunicipio.Text = controlMunicipio.obterNome(Int32.Parse(tfIdMunicipio.Text));
                }
                if (gridItems.Rows[gridItems.SelectedCells[0].RowIndex].Cells["atleta"].Value.ToString() == "Sim")
                {
                    habilitaTudoAtleta();
                    tfPeso.Text = gridItems.Rows[gridItems.SelectedCells[0].RowIndex].Cells["peso"].Value.ToString();
                    tfAltura.Text = gridItems.Rows[gridItems.SelectedCells[0].RowIndex].Cells["altura"].Value.ToString();
                    cbTamHelmet.SelectedIndex = controlPessoa.retornaTamanhoInt(gridItems.Rows[gridItems.SelectedCells[0].RowIndex].Cells["tam_helmet"].Value.ToString());
                    cbTamShoulder.SelectedIndex = controlPessoa.retornaTamanhoInt(gridItems.Rows[gridItems.SelectedCells[0].RowIndex].Cells["tam_shoulder"].Value.ToString());
                    cbTamJersey.SelectedIndex = controlPessoa.retornaTamanhoInt(gridItems.Rows[gridItems.SelectedCells[0].RowIndex].Cells["tam_jersey"].Value.ToString());
                    cbTamCalca.SelectedIndex = controlPessoa.retornaTamanhoInt(gridItems.Rows[gridItems.SelectedCells[0].RowIndex].Cells["tam_calca"].Value.ToString());
                    numCalcado.Value = Int32.Parse(gridItems.Rows[gridItems.SelectedCells[0].RowIndex].Cells["tam_pe"].Value.ToString());
                    cbIsentoMens.SelectedItem = gridItems.Rows[gridItems.SelectedCells[0].RowIndex].Cells["isento_mensalidade"].Value.ToString();
                    cbAtivo.SelectedItem = gridItems.Rows[gridItems.SelectedCells[0].RowIndex].Cells["ativo"].Value.ToString();
                    tfIdNacionalidade.Text = gridItems.Rows[gridItems.SelectedCells[0].RowIndex].Cells["nacionalidade_id"].Value.ToString();
                    if (!String.IsNullOrWhiteSpace(tfIdNacionalidade.Text))
                    {
                        tfNomeNacionalidade.Text = controlNacionalidade.obterNome(Int32.Parse(tfIdNacionalidade.Text));
                    }
                } else
                {
                    cbAtleta.SelectedIndex = 0;
                }
                
                tfNome.Focus();
            }
            else
            {
                MessageBox.Show("Por favor, selecione um registro.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void BtnExcluir_Click(object sender, EventArgs e)
        {
            if (gridItems.SelectedCells.Count > 0)
            {
                if (MessageBox.Show("Tem certeza que deseja excluir o registro?", "Aviso", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    bool result = controlPessoa.remove(Int32.Parse(gridItems.Rows[gridItems.SelectedCells[0].RowIndex].Cells["Id"].Value.ToString()));                    
                    if (!result)
                    {
                        MessageBox.Show("Ocorreu um erro ao excluir o registro.", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    estadoInicial();
                }
            }
            else
            {
                MessageBox.Show("Por favor, selecione um registro.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

        }

        private void BtnSalvar_Click_1(object sender, EventArgs e)
        {
            if (validaCampos())
            {
                int id_municipio = 0;
                int id_nacionalidade = 0;
                float peso = 0, altura = 0;
                if (!string.IsNullOrWhiteSpace(tfIdMunicipio.Text))
                {
                    id_municipio = Int32.Parse(tfIdMunicipio.Text);
                }
                if (!string.IsNullOrWhiteSpace(tfIdNacionalidade.Text))
                {
                    id_nacionalidade = Int32.Parse(tfIdNacionalidade.Text);
                }
                if (!String.IsNullOrWhiteSpace(tfPeso.Text))
                {
                    peso = float.Parse(tfPeso.Text);

                }
                if (!String.IsNullOrWhiteSpace(tfAltura.Text))
                {
                    altura = float.Parse(tfAltura.Text);
                }

                if (isAtualizaCadastro)
                {
                    if (!string.IsNullOrWhiteSpace(tfIdMunicipio.Text))
                    {
                        id_municipio = Int32.Parse(tfIdMunicipio.Text);
                    }
                    if (!string.IsNullOrWhiteSpace(tfIdNacionalidade.Text))
                    {
                        id_nacionalidade = Int32.Parse(tfIdNacionalidade.Text);
                    }                    
                    bool result = controlPessoa.atualiza(new Pessoa(Int32.Parse(tfID.Text), tfNome.Text, controlPessoa.retornaTipoPessoaChar(cbTipoPessoa.SelectedIndex), tfCPF.Text, tfRG.Text, dtNasc.Value, tfCNPJ.Text, tfInscEst.Text, tfCEP.Text, tfLogradouro.Text, tfNumero.Text, tfBairro.Text, id_municipio, cbAtleta.SelectedIndex, peso, altura, controlPessoa.retornaTamanhoString(cbTamHelmet.SelectedIndex), controlPessoa.retornaTamanhoString(cbTamJersey.SelectedIndex), controlPessoa.retornaTamanhoString(cbTamJersey.SelectedIndex), controlPessoa.retornaTamanhoString(cbTamCalca.SelectedIndex), Int32.Parse(numCalcado.Value.ToString()), cbIsentoMens.SelectedIndex, cbAtivo.SelectedIndex, id_nacionalidade));
                    if (!result)
                    {
                        MessageBox.Show("Ocorreu um erro ao atualizar o registro.", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                    {
                        estadoInicial();
                    }
                }
                else
                {

                    int result = controlPessoa.insere(new Pessoa(tfNome.Text, controlPessoa.retornaTipoPessoaChar(cbTipoPessoa.SelectedIndex), tfCPF.Text, tfRG.Text, dtNasc.Value, tfCNPJ.Text, tfInscEst.Text, tfCEP.Text, tfLogradouro.Text, tfNumero.Text, tfBairro.Text, id_municipio, cbAtleta.SelectedIndex, peso, altura, controlPessoa.retornaTamanhoString(cbTamHelmet.SelectedIndex), controlPessoa.retornaTamanhoString(cbTamJersey.SelectedIndex), controlPessoa.retornaTamanhoString(cbTamJersey.SelectedIndex), controlPessoa.retornaTamanhoString(cbTamCalca.SelectedIndex), Int32.Parse(numCalcado.Value.ToString()), cbIsentoMens.SelectedIndex, cbAtivo.SelectedIndex, id_nacionalidade));
                    if (result < 1)
                    {
                        MessageBox.Show("Ocorreu um erro ao inserir o registro.", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                    {
                       estadoInicial();
                    }
                }
                
            } else
            {
                MessageBox.Show("Por Favor, preencha o campo de município.", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void cbAtleta_SelectedValueChanged_1(object sender, EventArgs e)
        {
            if (cbAtleta.SelectedIndex == -1)
            {
            }
            else
            {
                if (cbAtleta.SelectedItem.ToString() == "Sim")
                {
                    habilitaTudoAtleta();
                    tabControl1.SelectedIndex = 1;
                    tfPeso.Focus();
                }
                else
                {
                    desabilitaTudoAtleta();
                }
            }
        }


        private void TfPeso_KeyPress(object sender, KeyPressEventArgs e)
        {
            Utils.formataNumericEvent(sender, e);
        }

        private void TfAltura_KeyPress(object sender, KeyPressEventArgs e)
        {
            Utils.formataNumericEvent(sender, e);
        }

        private void TfFiltro_TextChanged(object sender, EventArgs e)
        {
            Utils.filtraGrid(gridItems, cbFiltro, tfFiltro);
        }

        private void BtnBuscarMunicipio_Click(object sender, EventArgs e)
        {
            frmBusca busca = new frmBusca(controlMunicipio.obter());
            busca.Text = busca.Text + "Município";
            busca.ShowDialog();
            tfIdMunicipio.Text = busca.id_selecionado.ToString();
            tfNomeMunicipio.Text = controlMunicipio.obterNome(busca.id_selecionado);
            busca = null;
        }

        private void BtnBuscarNacionalidade_Click(object sender, EventArgs e)
        {
            frmBusca busca = new frmBusca(controlNacionalidade.obter());
            busca.Text = busca.Text + "Nacionalidade";
            busca.ShowDialog();
            tfIdNacionalidade.Text = busca.id_selecionado.ToString();
            tfNomeNacionalidade.Text = controlNacionalidade.obterNome(busca.id_selecionado);
            busca = null;
        }

        private void TfIdMunicipio_Leave(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(tfIdMunicipio.Text))
            {
                tfNomeMunicipio.Text = controlMunicipio.obterNome(Int32.Parse(tfIdMunicipio.Text));
            }
        }

        private void TfIdNacionalidade_Leave(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(tfIdNacionalidade.Text))
            {
                tfNomeNacionalidade.Text = controlNacionalidade.obterNome(Int32.Parse(tfIdNacionalidade.Text));
            }
        }

        private void CbAtleta_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
